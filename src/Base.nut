::g <- {};

::g.Base <- {

    "mCurrentEditedBlock": null,
    "mObjectInspector": null,

    "mCompositor": null,

    "mCurrentSavePath": null,
    "mToolbar": null,

    function setup(){
        //Number used each time I need a new datablock.
        ::globalCount <- 0;

        setupActionSets();

        _gui.loadSkins("res://res/skin.json");

        _doFile("res://src/project/ProjectBlockId.nut");
        mCurrentEditedBlock = ::BlockId();

        _doFile("res://src/action/ActionManager.nut");
        _doFile("res://src/project/Project.nut");
        local projPath = ::g.getDefaultFilePath();
        ::g.Project.setup(projPath);

        //_doFile("res://src/gui/MainOptionsGui.nut");
        _doFile("res://src/gui/BlockViewerGui.nut");
        _doFile("res://src/gui/ObjectInspector.nut");
        _doFile("res://src/gui/Toolbar.nut");
        //local mainOptions = ::g.MainOptionsGui();

        mToolbar = ::g.Toolbar();

        local blockViewer = ::g.BlockViewerGui();

        local objectInspector = ::g.ObjectInspector();
        objectInspector.setupGui();
        objectInspector.setupScene();
        mObjectInspector = objectInspector;

        _doFile("res://src/gui/BlockEditorGui.nut");
        local blockEditorGui = ::g.BlockEditorGui();
        blockEditorGui.setCurrentEditedBlock(null);

        //Note temp
        //::g.Project.createBlock("test pbs", BlockType.PBS);
        //::g.Project.createBlock("test blend", BlockType.BLEND);
        //::g.Project.createBlock("test macro", BlockType.MACRO);

        mCompositor = _compositor.addWorkspace([_window.getRenderTexture()], _camera.getCamera(), "matEd/gui/RenderToWindowWorkspace", true);

        _gui.setCanvasSize(Vec2(1920, 1080), Vec2(_window.getWidth(), _window.getHeight()));

        //Note temp
        _gui.setScrollSpeed(5.0);

    }

    function saveProject(){
        local projPath = ::g.getDefaultFilePath();
        ::g.Project.saveToFile(projPath);
    }

    function setupActionSets(){
        _input.setActionSets({
            "default" : {
                "Buttons" : {
                    "RotateLights": "#RotateLights"
                }
            }
        });

        ::Input <- {};
        ::Input.rotateLights <- _input.getButtonActionHandle("RotateLights");

        _input.mapKeyboardInput(_K_G, ::Input.rotateLights);
    }

    //TODO Ideally I'd be passing the blockId rather than the two values.
    function setCurrentEditedBlock(blockType, localId = null){
        print("Setting edited block to " + blockType + " " + localId);

        local block = null;
        if(blockType == null){
            mCurrentEditedBlock = ::BlockId();
        }else{
            mCurrentEditedBlock = ::BlockId(blockType, localId);
            block = getCurrentEditedBlock();
            if(block == null){
                mCurrentEditedBlock = ::BlockId();
            }
        }
        _event.transmit(Events.EDITED_BLOCK_CHANGED, block);
    }

    function getCurrentEditedBlock(){
        if(mCurrentEditedBlock == null) return null;
        if(mCurrentEditedBlock.isValid()) return null;

        return ::g.Project.getBlockForBlockId(mCurrentEditedBlock);
    }

    function update(){
        mObjectInspector.update();
    }


};

//Util functions.
::g.getBlockName <- function(blockType){
    switch(blockType){
        case BlockType.PBS:
            return "PBS Datablock";
        case BlockType.UNLIT:
            return "Unlit Datablock";
        case BlockType.MACRO:
            return "Macroblock";
        case BlockType.BLEND:
            return "Blendblock";
        case BlockType.SAMPLER:
            return "Samplerblock";
    }
}

::g.getDefaultFilePath <- function(){
    local startPath = _settings.getUserSetting("StartFile");
    if(startPath == null){
        startPath = "/tmp/test.material.json"
    }
    return startPath
}

