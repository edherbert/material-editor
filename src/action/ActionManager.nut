::g.ActionManager <- {

    //Stacks for the undo redo system.
    //The back of the array is considered the top of the stack.
    "__mUndoStack": [],
    "__mRedoStack": [],

    function setup(){

    }

    function undo(){
        if(__mUndoStack.len() > 0){
            local action = __mUndoStack.pop();
            __mRedoStack.push(action);
            action.performAntiAction();
        }
    }

    function redo(){
        if(__mRedoStack.len() > 0){
            local action = __mRedoStack.pop();
            __mUndoStack.push(action);
            action.performAction();
        }

    }

    function pushAndPerform(action){
        print("Pushed action");
        __mUndoStack.append(action);
        action.performAction();

        __mRedoStack.clear();
    }
};

_doFile("res://src/action/actions/BlockLifecycleAction.nut");
_doFile("res://src/action/actions/PBSDatablockAction.nut");
_doFile("res://src/action/actions/BlendblockAction.nut");
_doFile("res://src/action/actions/MacroblockAction.nut");
