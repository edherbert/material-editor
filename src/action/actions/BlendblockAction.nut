enum BlendblockAction{
    BOOLEAN_CHANGE,
    FACTOR_CHANGE,
    OPERATION_CHANGE
};

enum BlendblockActionFactorChange{
    SRC,
    DEST,
    SRC_ALPHA,
    SRC_DEST
};

enum BlendblockActionBlendOperationChange{
    OPERATION,
    ALPHA
};

enum BlendblockActionBooleanChange{
    ALPHA_TO_COVERAGE,
    SEPARATE_BLEND
};

::g.ActionManager.BlendblockAction <- class{

    firstVal = null;
    secondVal = null;
    thirdVal = null;
    mTargetBlock = null;
    mActionType = null;

    constructor(){

    }

    function populateBooleanChange(targetBlock, type, newMode, oldMode){
        firstVal = newMode;
        secondVal = oldMode;
        thirdVal = type;
        mTargetBlock = targetBlock;
        mActionType = BlendblockAction.BOOLEAN_CHANGE;
    }

    function populateBlendFactor(targetBlock, type, newFactor, oldFactor){
        firstVal = newFactor;
        secondVal = oldFactor;
        thirdVal = type;
        mTargetBlock = targetBlock;
        mActionType = BlendblockAction.FACTOR_CHANGE;
    }

    function populateBlendOperation(targetBlock, type, newOp, oldOp){
        firstVal = newOp;
        secondVal = oldOp;
        thirdVal = type;
        mTargetBlock = targetBlock;
        mActionType = BlendblockAction.OPERATION_CHANGE;
    }

    function performAction(){
        assert(mTargetBlock != null);
        local blockInstance = ::g.Project.getBlockForBlockId(mTargetBlock);
        assert(blockInstance != null);

        _perform(blockInstance, firstVal);
    }

    function performAntiAction(){
        local blockInstance = ::g.Project.getBlockForBlockId(mTargetBlock);
        assert(blockInstance != null);

        _perform(blockInstance, secondVal);
    }

    function _perform(target, val){
        switch(mActionType){
            case BlendblockAction.BOOLEAN_CHANGE:{
                if(thirdVal == BlendblockActionBooleanChange.ALPHA_TO_COVERAGE) target.setAlphaToCoverage(val);
                else if(thirdVal == BlendblockActionBooleanChange.SEPARATE_BLEND) target.setSeperateBlend(val);
                else assert(false);
                break;
            }
            case BlendblockAction.FACTOR_CHANGE:{
                target.setBlendFactor(thirdVal, val);
                break;
            }
            case BlendblockAction.OPERATION_CHANGE:{
                target.setBlendOperation(thirdVal, val);
                break;
            }
        }
    }
};
