enum PBSAction{
    MACRO_CHANGE,
    BLEND_CHANGE,

    SHADOW_CONST_BIAS_CHANGE,
    WORKFLOW_CHANGE,

    TEXTURE_CHANGE,
    FLOAT_THREE_CHANGE,
    UV_CHANGE,
    FLOAT_SINGLE_CHANGE,

    FRESNEL_MODE_CHANGE,
    SET_TRANSPARENCY_MODE,
    TRANSPARENCY_USE_ALPHA_FROM_TEXTURES,

    DETAIL_MODE_CHANGE,
    DETAIL_SET_WEIGHT
};

enum PBSActionSingleValue{
    TRANSPARENCY,
    ROUGHNESS,
    METALNESS,
    NORMAL,
    EMISSIVE
};

::g.ActionManager.PBSDatablockAction <- class{

    firstVal = null;
    secondVal = null;
    thirdVal = null;
    mTargetBlock = null;
    mActionType = null;

    constructor(){

    }

    function getFresnelModeNew(value){
        if(value == 0) return PBSFresnelMode.coeff;
        else if(value == 1) return PBSFresnelMode.ior;
        else if(value == 2) return PBSFresnelMode.coloured;
        else if(value == 3) return PBSFresnelMode.coloured_ior;
        else assert(false);
    }

    function populateDetailLayerSetMode(targetBlock, id, value, oldValue){
        mTargetBlock = targetBlock;
        firstVal = value;
        secondVal = oldValue;
        thirdVal = id;
        mActionType = PBSAction.DETAIL_MODE_CHANGE;
    }

    function populateDetailLayerSetWeight(targetBlock, id, value, oldValue){
        mTargetBlock = targetBlock;
        firstVal = value;
        secondVal = oldValue;
        thirdVal = id;
        mActionType = PBSAction.DETAIL_SET_WEIGHT;
    }

    function populateFresnelModeChange(targetBlock, value, oldValue){
        mTargetBlock = targetBlock;
        firstVal = getFresnelModeNew(value);
        secondVal = getFresnelModeNew(oldValue);
        mActionType = PBSAction.FRESNEL_MODE_CHANGE;
    }

    function populateSingleFloat(targetBlock, type, value, oldValue){
        mTargetBlock = targetBlock;
        thirdVal = type;
        firstVal = value;
        secondVal = oldValue;
        mActionType = PBSAction.FLOAT_SINGLE_CHANGE;
    }

    function populateGlobalBlockSetting(targetBlock, type, newBlockId, oldBlockId){
        mActionType = type;

        switch(type){
            case PBSAction.MACRO_CHANGE:
            case PBSAction.BLEND_CHANGE:
                firstVal = newBlockId;
                secondVal = oldBlockId;
                break;
            default:{
                assert(false);
            }
        }
        mTargetBlock = targetBlock;
    }

    function populateUVChange(targetBlock, type, newValue, oldValue){
        mTargetBlock = targetBlock;
        firstVal = newValue;
        secondVal = oldValue;
        mActionType = PBSAction.UV_CHANGE;
        thirdVal = type;
    }

    function populateGlobalWorkflow(targetBlock, newValue, oldValue){
        firstVal = newValue;
        secondVal = oldValue;
        mTargetBlock = targetBlock;
        mActionType = PBSAction.WORKFLOW_CHANGE;

        print("new value: " + newValue);
        print("old value: " + oldValue);
    }


    function populateSetTexture(targetBlock, actionType, pbsTexType, texName, oldTexName){
        firstVal = texName;
        secondVal = oldTexName;
        thirdVal = pbsTexType;
        mTargetBlock = targetBlock;
        mActionType = actionType;
    }

    function populate3fColour(targetBlock, actionType, values, oldValues){
        firstVal = values;
        secondVal = oldValues;
        thirdVal = actionType;
        mTargetBlock = targetBlock;
        mActionType = PBSAction.FLOAT_THREE_CHANGE;
    }

    function populateSetTransparentMode(targetBlock, newMode, oldMode){
        firstVal = newMode;
        secondVal = oldMode;
        mTargetBlock = targetBlock;
        mActionType = PBSAction.SET_TRANSPARENCY_MODE;
    }
    function populateUseAlphaFromTexture(targetBlock, newValue){
        firstVal = newValue;
        secondVal = !newValue;
        mTargetBlock = targetBlock;
        mActionType = PBSAction.TRANSPARENCY_USE_ALPHA_FROM_TEXTURES;
    }


    function populateShadowConstBias(targetBlock, value, oldValue){
        firstVal = value;
        secondVal = oldValue;
        mTargetBlock = targetBlock;
        mActionType = PBSAction.SHADOW_CONST_BIAS_CHANGE;
    }

    function performAction(){
        assert(mTargetBlock != null);
        local blockInstance = ::g.Project.getBlockForBlockId(mTargetBlock);
        assert(blockInstance != null);
        print(blockInstance);

        _perform(blockInstance, firstVal);
    }

    function performAntiAction(){
        local blockInstance = ::g.Project.getBlockForBlockId(mTargetBlock);
        assert(blockInstance != null);

        _perform(blockInstance, secondVal);
    }

    function _perform(target, val){
        switch(mActionType){
            case PBSAction.MACRO_CHANGE:
                target.setMacroblock(val);
                break;
            case PBSAction.BLEND_CHANGE:
                target.setBlendblock(val);
                break;
            case PBSAction.WORKFLOW_CHANGE:
                target.setWorkflow(val);
                break;
            case PBSAction.TEXTURE_CHANGE:{
                target.setTexture(thirdVal, val)
                break;
            }
            case PBSAction.FLOAT_THREE_CHANGE:{
                if(thirdVal == PBSColourChanged.DIFFUSE){
                    target.setDiffuse(val);
                }
                else if(thirdVal == PBSColourChanged.SPECULAR){
                    target.setSpecular(val);
                }
                else if(thirdVal == PBSColourChanged.FRESNEL){
                    target.setFresnel(val);
                }
                break;
            }
            case PBSAction.UV_CHANGE:{
                target.setTextureUVSource(val, thirdVal);
                break;
            }
            case PBSAction.FLOAT_SINGLE_CHANGE:{
                if(thirdVal == PBSActionSingleValue.TRANSPARENCY) target.setTransparency(val);
                else if(thirdVal == PBSActionSingleValue.ROUGHNESS) target.setRoughness(val);
                else if(thirdVal == PBSActionSingleValue.METALNESS) target.setMetalness(val);
                else if(thirdVal == PBSActionSingleValue.NORMAL) target.setNormal(val);
                else assert(false);
                break;
            }
            case PBSAction.FRESNEL_MODE_CHANGE:{
                target.setFresnelMode(val);
                break;
            }
            case PBSAction.SET_TRANSPARENCY_MODE:{
                target.setTransparencyMode(val);
                break;
            }
            case PBSAction.SHADOW_CONST_BIAS_CHANGE:{
                target.setShadowConstBias(val);
                break;
            }
            case PBSAction.TRANSPARENCY_USE_ALPHA_FROM_TEXTURES:{
                target.setTransparencyAlphaFromTexture(val);
                break;
            }
            case PBSAction.DETAIL_MODE_CHANGE:{
                target.setDetailMapBlendMode(thirdVal, val);
                break;
            }
            case PBSAction.DETAIL_SET_WEIGHT:{
                target.setDetailMapWeight(thirdVal, val);
                break;
            }
        }
    }
};
