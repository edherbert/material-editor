enum MacroblockAction{
    SCISSOR_TEST_CHANGE,
    DEPTH_CHECK,
    DEPTH_WRITE,
    DEPTH_FUNCTION,
    DEPTH_BIAS_CONSTANT,
    DEPTH_BIAS_SLOPE_SCALE,
    CULL_MODE,
    POLYGON_MODE
};

::g.ActionManager.MacroblockAction <- class{

    firstVal = null;
    secondVal = null;
    thirdVal = null;
    mTargetBlock = null;
    mActionType = null;

    constructor(){

    }

    function populate(targetBlock, actionType, newMode, oldMode){
        firstVal = newMode;
        secondVal = oldMode;
        mTargetBlock = targetBlock;
        mActionType = actionType;
    }

    function performAction(){
        assert(mTargetBlock != null);
        local blockInstance = ::g.Project.getBlockForBlockId(mTargetBlock);
        assert(blockInstance != null);

        _perform(blockInstance, firstVal);
    }

    function performAntiAction(){
        local blockInstance = ::g.Project.getBlockForBlockId(mTargetBlock);
        assert(blockInstance != null);

        _perform(blockInstance, secondVal);
    }

    function _perform(target, val){
        print("Performing macroblock action");
        switch(mActionType){
            case MacroblockAction.SCISSOR_TEST_CHANGE:{
                target.setScissorTest(val);
                break;
            }
            case MacroblockAction.DEPTH_CHECK:{
                target.setDepthCheck(val);
                break;
            }
            case MacroblockAction.DEPTH_WRITE:{
                target.setDepthWrite(val);
                break;
            }
            case MacroblockAction.DEPTH_FUNCTION:{
                target.setDepthFunction(val);
                break;
            }
            case MacroblockAction.DEPTH_BIAS_CONSTANT:{
                target.setDepthBiasConstant(val);
                break;
            }
            case MacroblockAction.DEPTH_BIAS_SLOPE_SCALE:{
                target.setDepthBiasSlopeScale(val);
                break;
            }
            case MacroblockAction.CULL_MODE:{
                target.setCullMode(val);
                break;
            }
            case MacroblockAction.POLYGON_MODE:{
                target.setPolygonMode(val);
                break;
            }
            default:{
                assert(false);
                break;
            }
        }
    }
};
