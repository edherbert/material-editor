enum BlockLifecycleActionType{
    ADD,
    REMOVE
};

::g.ActionManager.BlockLifecycleAction <- class{

    blockType = null;
    blockName = null;

    createdBlockId = null;

    constructor(){

    }

    function populateBlockAdded(blockName, blockType){
        this.blockName = blockName;
        this.blockType = blockType;
    }

    function performAction(){
        print(blockName);
        print(blockType);
        createdBlockId = ::g.Project.createBlock(blockName, blockType);
    }

    function performAntiAction(){
        ::g.Project.removeBlock(createdBlockId);
    }
};
