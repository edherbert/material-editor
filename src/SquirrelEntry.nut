
function start(){

    _doFile/*Execute*/("res://src/Constants.nut");

    _doFile("res://src/callbacks/SystemCallbacks.nut");

    _doFile("res://src/gui/widgets/PopupModal.nut");
    _doFile("res://src/gui/widgets/ColourPicker.nut");
    _doFile("res://src/gui/widgets/WrappedSpinner.nut");
    _doFile("res://src/gui/widgets/SliderObject.nut");
    _doFile("res://src/gui/widgets/BlockCreationPopup.nut");
    _doFile("res://src/gui/widgets/BlockSelector.nut");
    _doFile("res://src/gui/widgets/ogre/OgreItemSelector.nut");
    _doFile("res://src/gui/widgets/ogre/TextureSelector.nut");
    _doFile("res://src/gui/widgets/ogre/MeshSelector.nut");
    _doFile("res://src/gui/widgets/NumericTextField.nut");

    _doFile("res://src/Base.nut");
    ::g.Base.setup();

}

function update(){
    ::g.Base.update();
}

function end(){

}
