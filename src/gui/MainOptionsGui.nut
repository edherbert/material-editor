::g.MainOptionsGui <- class{

    titleName = null;
    typeName = null;

    constructor(){
        _event.subscribe(Events.EDITED_BLOCK_CHANGED, receiveEditedBlockChanged, this);

        local layout = _gui.createLayoutLine();

        local win = _gui.createWindow();
        win.setPosition(800, 0);
        win.setSize(200, 300);
        win.setZOrder(30);

        local mesh = MeshSelector(win)
        mesh.attachSelectionListener(function(val, oldVal){
            _event.transmit(Events.VIEWED_MESH_CHANGED, val);
        }, this);

        local buttons = [
            ["undo", function(widget, action){
                if(action != 2) return;
                print("undo button");
                ::g.ActionManager.undo();
            }],
            ["redo", function(widget, action){
                if(action != 2) return;
                print("redo button");
                ::g.ActionManager.redo();
            }],
            ["clear selection", function(widget, action){
                if(action != 2) return;
                ::g.Base.setCurrentEditedBlock(null);
            }],
            ["save", function(widget, action){
                if(action != 2) return;
                ::g.Base.saveProject();
            }]
        ];

        titleName = win.createLabel();
        titleName.setText(" ");
        layout.addCell(titleName);

        typeName = win.createLabel();
        typeName.setText(" ");
        layout.addCell(typeName);


        foreach(i,c in buttons){
            local button = win.createButton();
            button.setText(c[0]);
            layout.addCell(button);
            button.attachListener(c[1]);
        }

        layout.layout();
    }

    function receiveEditedBlockChanged(id, data){
        setDatablockToData(data);
    }

    function setDatablockToData(block){
        if(block == null){
            titleName.setText(" ");
            typeName.setText(" ");
        }else{
            titleName.setText(block.mName);
            typeName.setText(::g.getBlockName(block.mBlockType));
        }
    }

};
