::g.BlockEditorGui <- class{

    win = null;
    mCurrentBlock = null;
    mCurrentContent = null;

    constructor(){
        _event.subscribe(Events.EDITED_BLOCK_CHANGED, receiveBlocksChangedEvent, this);
        _event.subscribe(Events.BLOCK_VALUE_CHANGED, receiveBlockValueChanged, this);

        win = _gui.createWindow();
        win.setZOrder(30);
        win.setPosition(BLOCK_VIEWER_GUI_WIDTH, TOOLBAR_GUI_HEIGHT);
        win.setSize(BLOCK_EDITOR_GUI_WIDTH, BLOCK_EDITOR_GUI_HEIGHT);
    }

    /**
    Set the current block which is being edited by the gui.
    This must be a block instance.
    If null is provided this is considered that no block is selected.
    */
    function setCurrentEditedBlock(block){
        if(mCurrentContent != null){
            mCurrentContent.destroy();
        }

        mCurrentBlock = block;

        if(block == null){
            mCurrentContent = ::g.EmptyBlockEditorGui(win, mCurrentBlock);
            mCurrentContent.generate();
            return;
        }

        //Regenerate the window content.
        local type = block.mBlockType;
        switch(type){
            case BlockType.PBS:
                print("Creating pbs datablock");
                mCurrentContent = ::g.PBSDatablockEditorGui(win, mCurrentBlock);
                break;
            case BlockType.BLEND:
                print("Creating blendblock");
                mCurrentContent = ::g.BlendblockEditorGui(win, mCurrentBlock);
                break;
            case BlockType.MACRO:
                print("Creating macroblock");
                mCurrentContent = ::g.MacroblockEditorGui(win, mCurrentBlock);
                break;
            default:{
                assert(false);
            }
        }
        mCurrentContent.generate();
        mCurrentContent.setEntriesForBlock(mCurrentBlock);
    }

    function receiveBlocksChangedEvent(event, data){
        setCurrentEditedBlock(data);
    }

    function receiveBlockValueChanged(event, data){
        if(mCurrentContent == null) return;
        mCurrentContent.setEntriesForBlock(data);
    }

};

_doFile("res://src/gui/blocks/ObjectEditorGui.nut");
_doFile("res://src/gui/blocks/EmptyBlockEditorGui.nut");
_doFile("res://src/gui/blocks/PBSDatablockEditorGui.nut");
_doFile("res://src/gui/blocks/BlendblockEditorGui.nut");
_doFile("res://src/gui/blocks/MacroblockEditorGui.nut");
