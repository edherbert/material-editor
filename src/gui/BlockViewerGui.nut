::g.BlockViewerGui <- class{

    win = null;

    mBlockListerInstance = null;

    constructor(){
        //TODO unsubscribe from this.
        _event.subscribe(Events.BLOCKS_CHANGED, receiveBlocksChangedEvent, this);

        win = _gui.createWindow();
        win.setZOrder(40);
        win.setSize(BLOCK_VIEWER_GUI_WIDTH, 1080-TOOLBAR_GUI_HEIGHT);
        win.setPosition(0, TOOLBAR_GUI_HEIGHT);

        mBlockListerInstance = BlockListerWindow();

        local layout = _gui.createLayoutLine();

        local title = win.createLabel();
        title.setDefaultFont(3);
        title.setText("Blocks");
        //registerWidget(title, layout);
        layout.addCell(title);

        local createBlockButton = win.createButton();
        createBlockButton.setText("Create block");
        //registerWidget(createBlockButton, layout);
        layout.addCell(createBlockButton);
        createBlockButton.attachListener(function(widget, action){
            if(action != 2) return;
            local newBlock = BlockCreationPopup(null);
            newBlock.showPopup(true);
        });

        mBlockListerInstance.reGenerateEntries(win, blockSelectorListener);

        layout.layout();
    }

    function blockSelectorListener(widget, action){
        if(action != 2) return;

        local value = widget.getUserId();
        local blockType = (value >> 16);
        local blockId = value & 0xFFFF;

        ::g.Base.setCurrentEditedBlock(blockType, blockId);
    }

    function receiveBlocksChangedEvent(id, data){
        print("blocks changed");
        mBlockListerInstance.reGenerateEntries(win, blockSelectorListener);
    }

};

::g.BlockViewerGui.BlockListerWindow <- class{

    innerWin = null;

    guiItems = null;

    entryNames = null;
    callbacksArray = null;
    entryTypes = null;

    constructor(targetBlockType = null){
        guiItems = [];

        switch(targetBlockType){
            case BlockType.PBS:{
                entryNames = ["PBS"];
                callbacksArray = [::g.Project.mPBSBlendBlocks];
                break;
            }
            case BlockType.UNLIT:{
                entryNames = ["Unlit"];
                callbacksArray = [::g.Project.mUnlitBlendBlocks];
                break;
            }
            case BlockType.MACRO:{
                entryNames = ["Macroblocks"];
                callbacksArray = [::g.Project.mMacroBlocks];
                break;
            }
            case BlockType.BLEND:{
                entryNames = ["Blendblocks"];
                callbacksArray = [::g.Project.mBlendBlocks];
                break;
            }
            case BlockType.SAMPLER:{
                entryNames = ["Samplerblocks"];
                callbacksArray = [::g.Project.mSamplerBlocks];
                break;
            }
            case null:{
                entryNames = ["PBS", "Unlit", "Macroblocks", "Blendblocks", "Samplerblocks"];
                callbacksArray = [
                    ::g.Project.mPBSBlendBlocks,
                    ::g.Project.mUnlitBlendBlocks
                    ::g.Project.mMacroBlocks,
                    ::g.Project.mBlendBlocks,
                    ::g.Project.mSamplerBlocks,
                ];
            }
        }

        if(targetBlockType == null){
            entryTypes = [
                BlockType.PBS,
                BlockType.UNLIT,
                BlockType.MACRO,
                BlockType.BLEND,
                BlockType.SAMPLER
            ];
        }else{
            entryTypes = [targetBlockType];
        }
    }

    function registerWidget(widget, layout){
        layout.addCell(widget);
        guiItems.append(widget);
    }

    function reGenerateEntries(win, entryListener, listenerContext = null){
        //Clear any old entries first.
        destroyContent();

        innerWin = win.createWindow();
        innerWin.setPosition(0, 80);
        innerWin.setSize(280, 450);
        innerWin.setZOrder(11);
        innerWin.setDatablock("matEd/gui/invisible");

        local innerLayout = _gui.createLayoutLine();
        for(local i = 0; i < entryNames.len(); i++){
            local title = innerWin.createLabel();
            title.setDefaultFont(3);
            title.setText(entryNames[i]);
            registerWidget(title, innerLayout);

            for(local y = 0; y < callbacksArray[i].len(); y++){
            //foreach(y in callbacksArray[i]){
                local item = callbacksArray[i][y];
                if(item == null) continue;
                local button = innerWin.createButton();
                if(entryListener != null){
                    local genId = y;
                    genId = genId | (entryTypes[i] << 16);
                    button.setUserId(genId);
                    if(listenerContext == null){
                        button.attachListener(entryListener);
                    }else{
                        button.attachListener(entryListener, listenerContext);
                    }
                }
                button.setText(item.mName);
                registerWidget(button, innerLayout);
            }
        }
        innerLayout.layout();
        innerWin.sizeScrollToFit();
    }

    function destroyContent(){
        foreach(i in guiItems){
            _gui.destroy(i);
        }
        guiItems.clear();
    }

};
