enum NumericTextFieldType{
    INTEGER,
    FLOAT
};

::NumericTextField <- class{

    mWindow = null;
    mEditbox = null;
    mType = null;

    mOldValue = 0;
    mValue = 0;
    mListenerFunc = null;

    constructor(win, type = NumericTextFieldType.INTEGER){
        mWindow = win;
        mType = type;

        mEditbox = mWindow.createEditbox();
        mEditbox.setSize(100, 40);
        setValue(0);
        mEditbox.attachListener(editboxCallback, this);
    }

    function triggerListener(){
        if(mListenerFunc != null){
            mListenerFunc(mValue, mOldValue);
        }

        mOldValue = mValue;
    }

    function setValue(val){
        mValue = val;
        mOldValue = val;
        mEditbox.setText(mValue.tostring());
    }

    function attachValueChangeListener(listenerFunc, context){
        if(context == null){
            mListenerFunc = listenerFunc;
        }else{
            mListenerFunc = listenerFunc.bindenv(context);
        }
    }

    function editboxCallback(widget, action){
        if(action == 5){
            local value = mEditbox.getText();
            try{
                if(mType == NumericTextFieldType.INTEGER){
                    mValue = value.tointeger();
                }else{
                    mValue = value.tofloat();
                }
            }catch(e){
                return;
            }

            //The string does convert to a number so call the listener.
            triggerListener();
        }
    }

    function attachToLayout(layout){
        layout.addCell(mEditbox);
    }

};
