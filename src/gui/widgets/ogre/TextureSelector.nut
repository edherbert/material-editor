::TextureSelector <- class extends ::OgreItemSelector{

    constructor(win){
        base.constructor(win, "texture");

    }

    function setTexture(texName){
        setObject(texName);
    }

    function getCurrentObjects(){
        mCurrentLoadedObjects = _graphics.getLoadedTextures();
    }

};
