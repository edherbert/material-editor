::MeshSelector <- class extends ::OgreItemSelector{

    constructor(win){
        base.constructor(win, "Mesh");
    }

    function getCurrentObjects(){
        mCurrentLoadedObjects = _graphics.getLoadedMeshes();
    }
};
