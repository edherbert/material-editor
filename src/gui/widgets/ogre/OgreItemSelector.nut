::OgreItemSelector <- class extends ::PopupModal{

    innerWindow = null;
    mCurrentLoadedObjects = null;
    mListenerFunc = null;
    mSelectedObject = null;

    mTypeName = null;

    constructor(win, typeName){
        mTypeName = typeName;

        base.constructor(win);

        _setObject("");
    }

    function objectAttachmentListener(widget, action){
        if(action != 2) return;
        local objName = mCurrentLoadedObjects[widget.getUserId()];
        selectObject(objName);
    }

    function populatePopup(){
        local vertLayout = _gui.createLayoutLine();

        local titleLabel = windowPopup.createLabel();
        titleLabel.setText(mTypeName + " selector");
        vertLayout.addCell(titleLabel);

        local layout = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
        local cancelButton = windowPopup.createButton();
        cancelButton.setText("Cancel");
        cancelButton.attachListener(function(widget, action){
            if(action != 2) return;
            showPopup(false);
        }, this);
        layout.addCell(cancelButton);

        local clearButton = windowPopup.createButton();
        clearButton.setText("Clear");
        clearButton.attachListener(function(widget, action){
            if(action != 2) return;
            selectObject("");
        }, this);
        layout.addCell(clearButton);

        layout.layout();

        vertLayout.addCell(layout);

        vertLayout.layout();

        innerWindow = windowPopup.createWindow();
        innerWindow.setSize(400, 400);
        innerWindow.setPosition(0, 80);
        innerWindow.setDatablock("matEd/gui/invisible");

        local innerLayout = _gui.createLayoutLine();

        getCurrentObjects();

        for(local i = 0; i < mCurrentLoadedObjects.len(); i++){
            local button = innerWindow.createButton();
            button.setText(mCurrentLoadedObjects[i]);
            button.setUserId(i);
            button.attachListener(objectAttachmentListener, this);
            innerLayout.addCell(button);
        }

        innerLayout.layout();
        innerWindow.sizeScrollToFit();
    }

    function _setObject(objName){
        if(objName == ""){
            button.setText("Select " + mTypeName);
            mSelectedObject = null;
        }else{
            button.setText(objName);
            mSelectedObject = objName;
        }
    }

    function selectObject(objName){
        local oldName = mSelectedObject;
        _setObject(objName);
        showPopup(false);

        //Call the listener callback.
        if(mListenerFunc != null){
            mListenerFunc(objName, oldName);
        }
    }

    /**
    Set the current texture from an engine texture object.
    If the object is null the texture will be set to nothing.
    */
    function setObject(obj){
        local objName = "";
        if(obj != null && obj.isValid()){
            objName = obj.getName();
        }
        _setObject(objName);
    }

    function destroyPopup(){
        mCurrentLoadedObjects = null;
    }

    /*
    Attach a listener function which expects the type function(textureName, oldTextureName).
    */
    function attachSelectionListener(listenerFunc, context = null){
        if(context == null){
            mListenerFunc = listenerFunc;
        }else{
            mListenerFunc = listenerFunc.bindenv(context);
        }
    }


};
