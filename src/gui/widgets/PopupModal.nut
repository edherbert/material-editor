::PopupModal <- class{

    button = null;
    parentWindow = null;

    backgroundWindow = null;
    windowPopup = null;
    windowVisible = false;

    constructor(win){
        if(win != null){
            parentWindow = win;

            button = win.createButton();
            button.attachListener(function(widget, action){
                if(action != 2) return;
                showPopup(!this.windowVisible);
            }, this);
        }

        //showPopup(true);
    }

    /**
    Show or hide the popup.
    This will perform the necessary logic to create or destroy the window.
    */
    function showPopup(visible){
        if(visible){

            windowPopup = _gui.createWindow();
            windowPopup.setPosition(1920 / 2 - 500, 1080 / 2 - 500);
            windowPopup.setSize(500, 500);
            windowPopup.setZOrder(101);
            windowPopup.setHidden(false);

            backgroundWindow = _gui.createWindow();
            backgroundWindow.setPosition(0, 0);
            backgroundWindow.setSize(1920, 1080);
            backgroundWindow.setZOrder(100);
            backgroundWindow.setHidden(false);

            populatePopup();

        }else{
            destroyPopup();
            _gui.destroy(windowPopup);
            _gui.destroy(backgroundWindow);
            windowPopup = null;
            backgroundWindow = null;
        }
        windowVisible = visible;
    }

    /**
    Override this and it will be called when the popup is created.
    */
    function populatePopup(){

    }
    function destroyPopup(){

    }

    function attachToLayout(layout){
        layout.addCell(button);
    }

};
