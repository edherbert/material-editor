enum SliderObjectListener{
    //Similar to the colour picker options.
    VALUE_VIEWED,
    VALUE_CHANGED
};

::SliderObject <- class{

    mWindow = null;
    mSlider = null;

    mOldValue = 0;
    mValue = 0;
    mListenerFunc = null;

    mChangeActive = false;

    constructor(win){
        mWindow = win;

        mSlider = mWindow.createSlider();
        mSlider.setSize(300, 20);
        mSlider.setRange(0, 100);
        mSlider.attachListener(sliderCallback, this);
    }

    function triggerListener(type){
        if(mListenerFunc != null){
            mListenerFunc(type, mValue, mOldValue);
            if(type == SliderObjectListener.VALUE_CHANGED) print("Pushing value");
        }
    }

    function _setValueRaw(val){
        assert(val >= 0.0 && val <= 1.0);
        mSlider.setValue(val * 100);
    }
    function setValueRaw(val){
        _setValueRaw(val);
    }
    //Set the value but update the old and new vals..
    function setValue(val){
        _setValueRaw(val);
        mValue = val;
        mOldValue = val;
    }

    function attachValueChangeListener(listenerFunc, context){
        if(context == null){
            mListenerFunc = listenerFunc;
        }else{
            mListenerFunc = listenerFunc.bindenv(context);
        }
    }

    function sliderCallback(widget, action){
        //print(action);
        if(action == 2){
            //Edit begins.
            //assert(!mChangeActive);
            mChangeActive = true;
        }
        else if(action == 3){
            assert(mChangeActive);
            mChangeActive = false;
            //Commit the value.
            triggerListener(SliderObjectListener.VALUE_CHANGED);
            mOldValue = mValue;
        }
        else if(action == 5){
            //A temporary change but update all the values.
            mValue = mSlider.getValue() / 100;
            triggerListener(SliderObjectListener.VALUE_VIEWED);
        }
    }

    function attachToLayout(layout){
        layout.addCell(mSlider);
    }

};
