enum ColourPickerListener{
    //The colour changed through something like a slider, but was not confirmed.
    COLOUR_VIEWED,
    //The colour changed and should be used as part of an action.
    COLOUR_CHANGED
};

::ColourPicker <- class extends ::PopupModal{

    hexEditbox = null;

    //NOTE this contains the editboxes, might want to rename it.
    //TODO the update causes the rgb boxes to reset.
    rgbEditBoxes = null;
    rgbValues = null;
    //Old for actions.
    rgbValuesOld = null;
    colourPickerDatablock = null;
    colourPanel = null;

    mListenerFunc = null;

    constructor(win){
        rgbEditBoxes = [];
        rgbValues = [1.0, 1.0, 1.0];
        rgbValuesOld = [1.0, 1.0, 1.0];
        writeColourValues();

        base.constructor(win);

        _reprocessColours();
    }

    function setValueRaw(value){
        rgbValues[0] = value.x;
        rgbValues[1] = value.y;
        rgbValues[2] = value.z;
        writeColourValues();

        _reprocessColours();
    }

    function writeColourValues(){
        for(local i = 0; i < rgbValues.len(); i++){
            rgbValuesOld[i] = rgbValues[i];
        }
    }


    function triggerListener(type){
        if(mListenerFunc != null){
            //Check if a change has actually happened.
            local diff = false;
            for(local i = 0; i < rgbValues.len(); i++){
                if(rgbValues[i] != rgbValuesOld[i]){
                    diff = true;
                    break;
                }
            }
            if(diff) mListenerFunc(type, rgbValues, rgbValuesOld);
        }

        if(type == ColourPickerListener.COLOUR_CHANGED){
            //Write the old values over so they're up to date for the next action.
            writeColourValues();
        }
    }

    function applyHexidecimalValue(val){
        if(val.slice(0, 1) == "#") val = val.slice(1);
        val = val.toupper();
        //TODO do a regex here to check if the value matches.
        local hexChars = "0123456789ABCDEF";
        if(val.len() == 6){
            rgbValues[0] = ( (hexChars.find(val[0].tochar())*16) + hexChars.find(val[1].tochar()) ) / 255.0
            rgbValues[1] = ( (hexChars.find(val[2].tochar())*16) + hexChars.find(val[3].tochar()) ) / 255.0
            rgbValues[2] = ( (hexChars.find(val[4].tochar())*16) + hexChars.find(val[5].tochar()) ) / 255.0

            _reprocessColours();
        }
    }

    function hexStringToInteger(hs){
        hs = hs.tolower();
        if (hs.slice(0, 2) == "0x") hs = hs.slice(2);
        local i = 0;
        foreach (c in hs) {
            local n = c - '0';
            if (n > 9) n = ((n & 0x1F) - 7);
            i = (i << 4) + n;
        }
        return i;
    }

    function attachColourListener(listenerFunc, context){
        if(context == null){
            mListenerFunc = listenerFunc;
        }else{
            mListenerFunc = listenerFunc.bindenv(context);
        }
    }

    function getHexFromDiffuse(){
        return format("#%02x%02x%02x",
            floor(rgbValues[0]*255),
            floor(rgbValues[1]*255),
            floor(rgbValues[2]*255));
    }

    function colourValueEditboxCallback(widget, action){
        if(action == 5){
            print("rgb value changed " + widget.getUserId() + " to " + widget.getText());

            for(local i = 0; i < 3; i++){
                local v = rgbEditBoxes[i].getText();
                try{
                    rgbValues[i] = v.tofloat();
                }catch(e){
                    continue;
                }
            }
            reprocessColours();
        }
    }

    function _reprocessColours(){
        if(colourPickerDatablock != null){
            colourPickerDatablock.setColour(rgbValues[0], rgbValues[1], rgbValues[2], 1);
        }

        if(rgbEditBoxes.len() > 0){
            rgbEditBoxes[0].setText(rgbValues[0].tostring());
            rgbEditBoxes[1].setText(rgbValues[1].tostring());
            rgbEditBoxes[2].setText(rgbValues[2].tostring());
        }

        local hexValue = getHexFromDiffuse();
        button.setText(hexValue);
        if(hexEditbox) hexEditbox.setText(hexValue);
    }
    function reprocessColours(){
        print("colours changed to (" + rgbValues[0].tostring() + ", " + rgbValues[1].tostring() + ", " + rgbValues[2].tostring() + ")");
        _reprocessColours();

        triggerListener(ColourPickerListener.COLOUR_VIEWED);
    }

    function populatePopup(){
        local layout = _gui.createLayoutLine();

        local titleLabel = windowPopup.createLabel();
        titleLabel.setText("colour picker");
        layout.addCell(titleLabel);

        local horiz = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
        { //Create the rgb editboxes.
            local labels = ["red", "green", "blue"];
            for(local i = 0; i < 3; i++){
                local label = windowPopup.createLabel();
                label.setText(labels[i]);
                horiz.addCell(label);

                local editBox = windowPopup.createEditbox();
                editBox.setSize(100, 40);
                editBox.setText(rgbValues[i].tostring());
                editBox.setUserId(i);
                editBox.attachListener(colourValueEditboxCallback, this);
                horiz.addCell(editBox);
                rgbEditBoxes.append(editBox);
            }
            horiz.layout();
            layout.addCell(horiz);
        }

        //TODO find a better way to do this sizer stuff without breaking anything.
        local horizHex = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
        {
            hexEditbox = windowPopup.createEditbox();
            hexEditbox.setSize(180, 40);
            horizHex.addCell(hexEditbox);

            local doneButton = windowPopup.createButton();
            doneButton.setText("Apply");
            doneButton.attachListener(function(widget, action){
                if(action == 2) applyHexidecimalValue(hexEditbox.getText());
            }, this);
            horizHex.addCell(doneButton);
            horizHex.layout();

            layout.addCell(horizHex);
        }

        local countId = ::globalCount++;
        local macroblock = _hlms.getMacroblock({
            "polygonMode": 1,
        });
        local constructionInfo = {
            "diffuse": "1 1 1"
        };
        colourPickerDatablock = _hlms.unlit.createDatablock("colourPickerWindow" + countId, null, null, constructionInfo);

        colourPanel = windowPopup.createPanel();
        colourPanel.setSize(50, 50);
        colourPanel.setDatablock(colourPickerDatablock);
        layout.addCell(colourPanel);

        local doneButton = windowPopup.createButton();
        doneButton.setText("Done");
        doneButton.attachListener(function(widget, action){
            if(action == 2){
                showPopup(false);
                triggerListener(ColourPickerListener.COLOUR_CHANGED);
            }
        }, this);
        layout.addCell(doneButton);

        layout.layout();

        _reprocessColours();
    }

    function destroyPopup(){
        assert(colourPickerDatablock);
        print("destroying datablock");
        //_gui.destroy(colourPanel);
        //colourPanel = null;

        //Give the panel the default datablock so no assert triggers.
        colourPanel.setDatablock(_hlms.unlit.getDefaultDatablock());
        _hlms.destroyDatablock(colourPickerDatablock);
        colourPickerDatablock = null;

        rgbEditBoxes.clear();
        hexEditbox = null;

    }

};
