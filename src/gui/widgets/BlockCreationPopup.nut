::BlockCreationPopup <- class extends ::PopupModal{

    matchedValues = [
        BlockType.PBS,
        BlockType.UNLIT,

        BlockType.MACRO,
        BlockType.BLEND,
        BlockType.SAMPLER
    ];

    spinner = null;
    editBox = null;

    constructor(win){
        base.constructor(win);
    }

    function populatePopup(){
        local layout = _gui.createLayoutLine();

        local title = windowPopup.createLabel();
        title.setText("Create a block");
        layout.addCell(title);

        spinner = windowPopup.createSpinner();
        spinner.setOptions(["PBS", "Unlit", "Macroblock", "Blendblock", "Samplerblock"]);
        layout.addCell(spinner);

        local layoutHoriz = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
        {
            local title = windowPopup.createLabel();
            title.setText("Block name:");
            layoutHoriz.addCell(title);

            editBox = windowPopup.createEditbox();
            editBox.setSize(200, 40);
            editBox.setText("block");
            layoutHoriz.addCell(editBox);

            layoutHoriz.layout();
        }
        layout.addCell(layoutHoriz);

        local button = windowPopup.createButton();
        button.setText("create");
        layout.addCell(button);
        button.attachListener(function(widget, action){
            if(action != 2) return;
            showPopup(false);
            local action = ::g.ActionManager.BlockLifecycleAction();
            action.populateBlockAdded(editBox.getText(), matchedValues[spinner.getValueRaw()]);
            ::g.ActionManager.pushAndPerform(action);
        }, this);

        local cancelButton = windowPopup.createButton();
        cancelButton.setText("cancel");
        layout.addCell(cancelButton);
        cancelButton.attachListener(function(widget, action){
            if(action != 2) return;
            showPopup(false);
        }, this);

        layout.layout();
    }

    function destroyPopup(){

    }

};
