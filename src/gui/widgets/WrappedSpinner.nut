::WrappedSpinner <- class{

    mWindow = null;
    mSpinner = null;

    mOldValue = null;
    mValue = null;
    mListenerFunc = null;

    constructor(win, values){
        mWindow = win;

        mOldValue = 0;
        mValue = 0;

        mSpinner = mWindow.createSpinner();
        mSpinner.setOptions(values);
        mSpinner.attachListener(spinnerCallback, this);
    }

    function triggerListener(){
        if(mListenerFunc != null){
            mListenerFunc(mValue, mOldValue);
        }

        mOldValue = mValue;
    }

    function setValueRaw(val){
        mValue = val;
        mOldValue = val;
        mSpinner.setValueRaw(mValue);
    }

    function attachListener(listenerFunc, context){
        if(context == null){
            mListenerFunc = listenerFunc;
        }else{
            mListenerFunc = listenerFunc.bindenv(context);
        }
    }

    function spinnerCallback(widget, action){
        if(action == 5){
            local oldValue = mValue;
            mValue = widget.getValueRaw();
            triggerListener();
        }
    }

    function attachToLayout(layout){
        layout.addCell(mSpinner);
    }

};
