::BlockSelector <- class extends ::PopupModal{

    //innerWindow = null;
    currentLoadedTextures = null;
    mBlockType = null;

    mListenerFunc = null;

    mBlockLister = null;

    mCurrentSelectedBlock = null;

    constructor(win, blockType){
        base.constructor(win);
        mBlockType = blockType;

        mCurrentSelectedBlock = ::BlockId();

        mBlockLister = ::g.BlockViewerGui.BlockListerWindow(blockType);
        selectBlock(null);
    }

    /**
    Set the current blend block, passing a wrapped id.
    If null the block will be set to invalid.
    */
    function selectBlock(block){
        if(block == null){
            button.setText("Select " + ::g.getBlockName(mBlockType));
        }else{
            local blockInstance = ::g.Project.getBlockForBlockId(block);
            button.setText(blockInstance.mName);
        }
    }

    function blockSelectorListener(widget, action){
        if(action != 2) return;

        //TODO remove duplication from this function.
        local value = widget.getUserId();
        local blockType = (value >> 16);
        local blockId = value & 0xFFFF;

        local wrappedId = ::BlockId(blockType, blockId);
        //local blockInstance = ::g.Project.getBlockForBlockId(wrappedId);
        showPopup(false);
        local oldValue = mCurrentSelectedBlock;
        mCurrentSelectedBlock = wrappedId;

        selectBlock(wrappedId);

        if(mListenerFunc != null){
            mListenerFunc(wrappedId, oldValue);
        }
    }

    function populatePopup(){
        local layout = _gui.createLayoutLine();

        local titleLabel = windowPopup.createLabel();
        titleLabel.setText("Select " + ::g.getBlockName(mBlockType));
        layout.addCell(titleLabel);

        local closeButton = windowPopup.createButton();
        closeButton.setText("Close");
        layout.addCell(closeButton);
        closeButton.attachListener(function(widget, action){
            if(action != 2) return;
            showPopup(false);
        }, this);

        mBlockLister.reGenerateEntries(windowPopup, blockSelectorListener, this);

        layout.layout();
    }

    function attachSelectionListener(listenerFunc, context){
        if(context == null){
            mListenerFunc = listenerFunc;
        }else{
            mListenerFunc = listenerFunc.bindenv(context);
        }
    }

    function destroyPopup(){
        currentLoadedTextures = null;
        mBlockLister.destroyContent();
    }
};
