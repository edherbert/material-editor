::g.Toolbar <- class{

    mToolbarWindow = null;

    mDropDownWindow = null;
    mBackgroundWindow = null;

    mDataFunctions = null;

    function buttonListener(widget, action){
        if(action != 2) return;
        if(mDropDownWindow != null){
            //It's already open so close it
            closeDropdown();
            return;
        }

        local pos = widget.getPosition();
        pos.y += widget.getSize().y;
        showDropdown(widget.getUserId(), pos);
    }

    function backgroundButtonListener(widget, action){
        if(action != 2) return;
        print("background clicked");
        closeDropdown();
    }

    constructor(){
        mDataFunctions = [
            [
                ["save", function(widget, action){
                    if(action != 2) return;
                    ::g.Base.saveProject();
                    closeDropdown();
                }],
                ["selectMesh", function(widget, action){
                    if(action != 2) return;
                    ::g.Base.saveProject();
                    closeDropdown();
                }],
            ],
            [
                ["undo", function(widget, action){
                    if(action != 2) return;
                    ::g.ActionManager.undo();
                    closeDropdown();
                }],
                ["redo", function(widget, action){
                    if(action != 2) return;
                    ::g.ActionManager.redo();
                    closeDropdown();
                }],
            ]
        ];

        mToolbarWindow = _gui.createWindow();
        mToolbarWindow.setSize(1920, TOOLBAR_GUI_HEIGHT);
        mToolbarWindow.setZOrder(90);
        mToolbarWindow.setSkin("internal/WindowToolbarSkin");

        local layout = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
        local entries = ["file", "edit"];
        for(local i = 0; i < entries.len(); i++){
            local button = mToolbarWindow.createButton();
            button.setText(entries[i]);
            button.setUserId(i);
            button.attachListener(buttonListener, this);
            button.setSkinPack("internal/ToolBarButtonSkin");

            layout.addCell(button);
        }

        local mesh = MeshSelector(mToolbarWindow)
        mesh.attachSelectionListener(function(val, oldVal){
            _event.transmit(Events.VIEWED_MESH_CHANGED, val);
        });
        mesh.attachToLayout(layout);

        layout.layout();
    }

    function showBackgroundWindow(){
        mBackgroundWindow = _gui.createWindow();
        mBackgroundWindow.setSize(1920, 1080);
        mBackgroundWindow.setPosition(0, 0);
        mBackgroundWindow.setZOrder(89);
        mBackgroundWindow.setClipBorders(0, 0, 0, 0);
        mBackgroundWindow.setVisualsEnabled(false);

        local button = mBackgroundWindow.createButton();
        button.setSize(1920, 1080);
        button.attachListener(backgroundButtonListener, this);
        button.setVisualsEnabled(false);
    }

    function closeDropdown(){
        _gui.destroy(mDropDownWindow);
        _gui.destroy(mBackgroundWindow);

        mDropDownWindow = null;
        mBackgroundWindow = null;
    }

    function showDropdown(id, pos){
        if(mDropDownWindow != null){
            closeDropdown();
        }

        showBackgroundWindow();
        //TODO workaround
        pos += 10;
        mDropDownWindow = _gui.createWindow();
        mDropDownWindow.setSize(200, 200);
        mDropDownWindow.setZOrder(91);
        mDropDownWindow.setPosition(pos);
        mDropDownWindow.setClipBorders(0, 0, 0, 0);
        mDropDownWindow.setFocus();

        local layout = _gui.createLayoutLine();
        local data = mDataFunctions[id];
        for(local i = 0; i < data.len(); i++){
            local button = mDropDownWindow.createButton();
            button.setText(data[i][0]);
            button.attachListener(data[i][1], this);
            layout.addCell(button);
        }

        layout.layout();
    }
};
