enum PanelStates{
    IN_PANEL,
    OUT_PANEL,
    CLICKED
};

::g.ObjectInspector<- class{

    mouseX = 0;
    mouseY = 0;
    oldMouseX = 0;
    oldMouseY = 0;

    mCameraRotX = PI/2;
    mCameraRotY = 0;
    mDirLightRotX = 0;
    mDirLightRotY = 0;

    mCurrentAnim = null;

    scrollWheelStrength = 0.1;
    currentZoomLevel = 1.8;
    meshOriginPoint = null;

    mSceneNode = null;
    mMesh = null;
    mDirectionLight = null;

    mParentWin = null;
    mRenderPanel = null;

    mRenderSceneFirst = null;
    mNewTex = null;
    mCurrentPanelState = PanelStates.OUT_PANEL;

    constructor(){
        _event.subscribe(Events.EDITED_BLOCK_CHANGED, receiveEditedBlockChanged, this);
        _event.subscribe(Events.EDITED_BLOCK_REGENERATED, receiveEditedBlockRegenerated, this);
        _event.subscribe(Events.VIEWED_MESH_CHANGED, receiveMeshChange, this);
    }

    function panelListener(widget, action){
        if(action == 1){
            mCurrentPanelState = PanelStates.IN_PANEL;
        }
        else if(action == 0){
            mCurrentPanelState = PanelStates.OUT_PANEL;
        }
        else if(action == 2){
            mCurrentPanelState = PanelStates.CLICKED;
            oldMouseX = mouseX;
            oldMouseY = mouseY;
        }
        else if(action == 3){
            mCurrentPanelState = PanelStates.IN_PANEL;
            oldMouseX = mouseX;
            oldMouseY = mouseY;
        }
    }

    function setupGui(){
        local intendedWidth = 1920 - BLOCK_EDITOR_GUI_WIDTH - BLOCK_VIEWER_GUI_WIDTH;
        local intendedHeight = 1080 - TOOLBAR_GUI_HEIGHT;

        mParentWin = _gui.createWindow();
        mParentWin.setSize(intendedWidth, intendedHeight);
        mParentWin.setPosition(BLOCK_EDITOR_GUI_WIDTH + BLOCK_VIEWER_GUI_WIDTH, TOOLBAR_GUI_HEIGHT);
        mParentWin.setZOrder(20);
        mParentWin.setClipBorders(0, 0, 0, 0);

        mNewTex = _graphics.createTexture("matEd/gui/RenderSceneTexture");
        mNewTex.setResolution(intendedWidth, intendedHeight);
        mNewTex.scheduleTransitionTo(_GPU_RESIDENCY_RESIDENT);
        local mRenderSceneFirst = _compositor.addWorkspace([mNewTex], _camera.getCamera(), "matEd/gui/SceneToTexture_renderToTexture", true);

        local testDatablock = _hlms.unlit.createDatablock("matEd/gui/RenderSceneDatablock");
        testDatablock.setTexture(0, "matEd/gui/RenderSceneTexture");

        mRenderPanel = mParentWin.createPanel();
        mRenderPanel.setSize(intendedWidth, intendedHeight);
        mRenderPanel.setDatablock(testDatablock);
        mRenderPanel.attachListener(panelListener, this);

    }

    function setPosition(x, y){
        mParentWin.setPosition(x, y);
    }

    function setSize(width, height){
        mParentWin.setSize(width, height);
    }

    function getDefaultMesh(){
        local unique = _settings.getUserSetting("StartMesh");
        if(unique == null){
            return "SimpleObject.mesh"
        }
        return unique;
    }

    function _setupMeshInScene(){
        mCurrentAnim = null;

        local scenePath = _settings.getUserSetting("StartScene");
        if(scenePath != null){

            local animFile = _settings.getUserSetting("StartAnimFile");
            if(animFile == null){
                _scene.insertSceneFile(scenePath);
            }else{
                local data = _scene.parseSceneFile(scenePath);
                local animData = _scene.insertParsedSceneFileGetAnimInfo(data);

                try{
                    _animation.loadAnimationFile(animFile);

                    mCurrentAnim = _animation.createAnimation("testName", animData);
                }catch(e){
                    print("Error loading animation");
                    print(e);
                }
            }

            local meshRadius = 5;
            currentZoomLevel = meshRadius * 3.0;
            scrollWheelStrength = meshRadius * 0.1;
            meshOriginPoint = Vec3();
            mMesh = null;

            positionCameraToZoom(currentZoomLevel);
            return;
        }

        _setSceneObject(getDefaultMesh());
    }

    function setupScene(){
        _setupMeshInScene();

        local childNode = _scene.getRootSceneNode().createChildSceneNode();

        mDirectionLight = _scene.createLight();
        childNode.attachObject(mDirectionLight);

        mDirectionLight.setType(_LIGHT_DIRECTIONAL);
        mDirectionLight.setDirection(-1, -1, -1);
        mDirectionLight.setPowerScale(PI);

        local secondNode = _scene.getRootSceneNode().createChildSceneNode();
        local secondDirection = _scene.createLight();
        secondNode.attachObject(secondDirection);

        secondDirection.setType(_LIGHT_DIRECTIONAL);
        secondDirection.setDirection(1, 1, -1);
        secondDirection.setPowerScale(PI);


        _scene.setAmbientLight(0xFFFFFFFF, 0xFFFFFFFF, Vec3(0, 1, 0));

        positionDirectionLight();
    }

    function receiveMeshChange(id, data){
        _setSceneObject(data);
    }
    function _setSceneObject(meshName){
        if(mSceneNode != null){
            mSceneNode.destroyNodeAndChildren();
        }
        if(meshName == "") return;

        mSceneNode = _scene.getRootSceneNode().createChildSceneNode();
        //Check if the mesh actually exists.
        try{
            mMesh = _scene.createItem(meshName);
            mSceneNode.attachObject(mMesh);
        }catch(e){
            print("Unable to find mesh: " + e);
            mMesh = null;
        }

        if(mMesh != null){
            local meshAabb = mMesh.getLocalAabb();
            meshOriginPoint = meshAabb.getCentre();

            local meshRadius = mMesh.getLocalRadius();
            currentZoomLevel = meshAabb.getSize().y * 3.0;
            scrollWheelStrength = meshRadius * 0.1;
        }else{
            meshOriginPoint = Vec3();
        }

        positionCameraToZoom(currentZoomLevel);
    }

    function receiveEditedBlockRegenerated(id, data){
        print("Received block regenerated");
        if(mMesh == null) return;
        mMesh.setDatablock(data);
    }

    function receiveEditedBlockChanged(id, data){
        if(mMesh == null) return;
        if(data == null){
            local defaultBlock = _hlms.pbs.getDefaultDatablock();
            mMesh.setDatablock(defaultBlock);
            return;
        }

        if(data.mBlockType != BlockType.PBS && data.mBlockType != BlockType.UNLIT) return;

        mMesh.setDatablock(data.mEngineBlock);
    }

     function update(){
        if(mCurrentPanelState == PanelStates.OUT_PANEL) return;

        local pos = mParentWin.getPosition();
        local mousePos = _gui.getMousePosGui();
        mouseX = mousePos.x - pos.x;
        mouseY = mousePos.y - pos.y;

        local mouseScroll = _input.getMouseWheelValue();
        if(mouseScroll != 0){
            positionCameraToZoom(currentZoomLevel + (mouseScroll * scrollWheelStrength));
        }

        if(mCurrentPanelState == PanelStates.CLICKED){
            local deltaX = mouseX - oldMouseX;
            local deltaY = mouseY - oldMouseY;

            oldMouseX = mouseX;
            oldMouseY = mouseY;

            local valPressed = _input.getButtonAction(::Input.rotateLights, _INPUT_ANY, _KEYBOARD_INPUT_DEVICE);
            if(valPressed){
                mDirLightRotX += deltaX * 0.005;
                mDirLightRotY += deltaY * 0.005;

                positionDirectionLight();
            }else{
                mCameraRotX += deltaX * 0.005;
                mCameraRotY += deltaY * 0.005;

                positionCamera();
            }
        }
    }

    function positionDirectionLight(){
        local xPos = cos(mDirLightRotX)*currentZoomLevel;
        local yPos = sin(mDirLightRotY)*currentZoomLevel;
        local dir = Vec3(-xPos, -yPos, 0);
        dir.normalise();
        mDirectionLight.setDirection(dir);
    }

    function positionCameraToZoom(zoom){
        currentZoomLevel = zoom;

        positionCamera();
    }

    function positionCamera(){
        local xPos = cos(mCameraRotX)*currentZoomLevel;
        local yPos = sin(mCameraRotX)*currentZoomLevel;
        _camera.setPosition(xPos, this.meshOriginPoint.y, yPos);
        _camera.lookAt(this.meshOriginPoint);
    }

};
