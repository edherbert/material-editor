::g.EmptyBlockEditorGui <- class extends ::g.ObjectEditorGui{

    constructor(win, block){
        base.constructor(win, block);
    }

    function generate(){
        local emptyLabel = mWin.createLabel();
        emptyLabel.setText("No block selected");
        addWidget(emptyLabel);
    }
};
