::g.BlendblockEditorGui <- class extends ::g.ObjectEditorGui{

    mEntryWidgets = null;

    mSrcBlendFactorData = null;
    mDestBlendFactorData = null;
    mSrcAlphaBlendFactorData = null;
    mDestAlphaBlendFactorData = null;

    mBlendOperationData = null;
    mBlendOperationAlphaData = null;

    constructor(win, block){
        base.constructor(win, block);

        mSrcBlendFactorData = {"mBlock": mBlock, "mType": BlendblockActionFactorChange.SRC};
        mDestBlendFactorData = {"mBlock": mBlock, "mType": BlendblockActionFactorChange.DEST};
        mSrcAlphaBlendFactorData = {"mBlock": mBlock, "mType": BlendblockActionFactorChange.SRC_ALPHA};
        mDestAlphaBlendFactorData = {"mBlock": mBlock, "mType": BlendblockActionFactorChange.SRC_DEST};

        mBlendOperationData = {"mBlock": mBlock, "mType": BlendblockActionBlendOperationChange.OPERATION};
        mBlendOperationAlphaData = {"mBlock": mBlock, "mType": BlendblockActionBlendOperationChange.ALPHA};
    }

    function generate(){
        mEntryWidgets = {};

        local layout = _gui.createLayoutLine();

        local titleLabel = mWin.createLabel();
        titleLabel.setDefaultFont(3);
        titleLabel.setText("Blendblock");
        layout.addCell(titleLabel);
        addWidget(titleLabel);

        {
            local checkbox = mWin.createCheckbox();
            checkbox.setText("Alpha to coverage");
            checkbox.attachListener(function(widget, action){
                if(action != 3) return;
                local action = ::g.ActionManager.BlendblockAction();
                action.populateBooleanChange(mBlock.getWrappedId(), BlendblockActionBooleanChange.ALPHA_TO_COVERAGE, widget.getValue(), !widget.getValue());
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, checkbox, null);
            mEntryWidgets.alphaToCoverage <- checkbox;
        }

        {
            local checkbox = mWin.createCheckbox();
            checkbox.setText("Separate blend");
            checkbox.attachListener(function(widget, action){
                if(action != 3) return;
                local action = ::g.ActionManager.BlendblockAction();
                action.populateBooleanChange(mBlock.getWrappedId(), BlendblockActionBooleanChange.SEPARATE_BLEND, widget.getValue(), !widget.getValue());
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, checkbox, null);
            mEntryWidgets.separateBlend <- checkbox;
        }

        //TODO complete the blendmask editing!

        createBlendFactorSpinner("src blend factor", mSrcBlendFactorData, "srcBlendFactor", layout);
        createBlendFactorSpinner("dst blend factor", mDestBlendFactorData, "dstBlendFactor", layout);
        createBlendFactorSpinner("src alpha blend factor", mSrcAlphaBlendFactorData, "srcAlphaBlendFactor", layout);
        createBlendFactorSpinner("dst alpha blend factor", mDestAlphaBlendFactorData, "dstAlphaBlendFactor", layout);

        createBlendOperationSpinner("blend operation", mBlendOperationData, "blendOperation", layout);
        createBlendOperationSpinner("blend operation alpha", mBlendOperationAlphaData, "blendOperationAlpha", layout);

        layout.layout();
    }

    function createBlendFactorSpinner(title, context, id, layout){
        local spinner = WrappedSpinner(mWin, ["one" "zero" "dst_colour" "src_colour" "one_minus_dst_colour" "one_minus_src_colour" "dst_alpha" "src_alpha" "one_minus_dst_alpha" "one_minus_src_alpha"]);
        spinner.attachListener(function(value, oldValue){
            local action = ::g.ActionManager.BlendblockAction();
            action.populateBlendFactor(mBlock.getWrappedId(), mType, value, oldValue);
            ::g.ActionManager.pushAndPerform(action);
        }, context);
        addWidgetWithLabel(layout, spinner, title);
        mEntryWidgets[id] <- spinner;
    }

    function createBlendOperationSpinner(title, context, id, layout){
        local spinner = WrappedSpinner(mWin, ["add" "subtract" "reverse_subtract" "min" "max"]);
        spinner.attachListener(function(value, oldValue){
            local action = ::g.ActionManager.BlendblockAction();
            action.populateBlendOperation(mBlock.getWrappedId(), mType, value, oldValue);
            ::g.ActionManager.pushAndPerform(action);
        }, context);
        addWidgetWithLabel(layout, spinner, title);
        mEntryWidgets[id] <- spinner;
    }

    function setEntriesForBlock(block){
        mEntryWidgets.alphaToCoverage.setValue(block.mData.alpha_to_coverage);
        mEntryWidgets.separateBlend.setValue(block.mData.separate_blend);

        mEntryWidgets.srcBlendFactor.setValueRaw(block.mData.src_blend_factor);
        mEntryWidgets.dstBlendFactor.setValueRaw(block.mData.dst_blend_factor);
        mEntryWidgets.srcAlphaBlendFactor.setValueRaw(block.mData.src_alpha_blend_factor);
        mEntryWidgets.dstAlphaBlendFactor.setValueRaw(block.mData.dst_alpha_blend_factor);

        mEntryWidgets.blendOperation.setValueRaw(block.mData.blend_operation);
        mEntryWidgets.blendOperationAlpha.setValueRaw(block.mData.blend_operation_alpha);

return;
        mData = {
            "alpha_to_coverage" : false,
            "blendmask" : "rgba",
            "separate_blend" : true,
            "src_blend_factor" : 0,
            "dst_blend_factor" : 0,
            "src_alpha_blend_factor" : 0,
            "dst_alpha_blend_factor" : 0,
            "blend_operation" : 0,
            "blend_operation_alpha" : 0
        };

    }
};
