::g.ObjectEditorGui <- class{

    mParentWin = null;
    mWin = null;
    mBlock = null;
    mObjects = null;

    constructor(win, block){
        mParentWin = win;
        mBlock = block;
        mObjects = [];

        mWin = win.createWindow();
        mWin.setPosition(0, 0);
        local size = win.getSize();
        mWin.setSize(size.x - 50, size.y - 50);
        mWin.setAllowMouseScroll(false);
        mWin.setDatablock("matEd/gui/invisible");
    }

    function addWidgetWithLabel(mainLayout, widget, targetText){
        if(targetText != null){
            local layoutHorizontal = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
            //TODO To keep from starting issues if other layout lines list it and it was destroyed.
            //test.append(layoutHorizontal);
            mObjects.append(layoutHorizontal);

            local text = mWin.createLabel();
            text.setText(targetText);
            layoutHorizontal.addCell(text);
            //mObjects.append(text);

            if(typeof(widget) == "instance"){
                widget.attachToLayout(layoutHorizontal);
            }else{
                layoutHorizontal.addCell(widget);
                //mObjects.append(widget);
            }
            layoutHorizontal.layout();

            mainLayout.addCell(layoutHorizontal);
        }else{
            mainLayout.addCell(widget);
        }
    }

    function addWidget(widget){
        //mObjects.append(widget);
    }

    function destroy(){
        _gui.destroy(mWin);
    }

    function setEntriesForBlock(block){

    }

};
