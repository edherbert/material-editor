::g.PBSDatablockEditorGui <- class extends ::g.ObjectEditorGui{

    mEntryWidgets = null;

    mDiffuseData = null;
    mSpecularData = null;
    mEmissiveData = null;
    mRoughnessData = null;
    mMetalnessData = null;
    mNormalData = null;
    mDetailDiffuseData = null;

    constructor(win, block){
        base.constructor(win, block);

        mDiffuseData = {"mBlock": mBlock, "mType": PBSColourChanged.DIFFUSE};
        mSpecularData = {"mBlock": mBlock, "mType": PBSColourChanged.SPECULAR};
        mEmissiveData = {"mBlock": mBlock, "mType": PBSColourChanged.EMISSIVE};
        mRoughnessData = {"mBlock": mBlock, "mType": PBSActionSingleValue.ROUGHNESS};
        mMetalnessData = {"mBlock": mBlock, "mType": PBSActionSingleValue.METALNESS};
        mNormalData = {"mBlock": mBlock, "mType": PBSActionSingleValue.NORMAL};
        mDetailDiffuseData = [
            //TODO what does mCreated do?
            {"mBlock": mBlock, "mId": 0, "mCreated": true},
            {"mBlock": mBlock, "mId": 1, "mCreated": true},
            {"mBlock": mBlock, "mId": 2, "mCreated": true},
            {"mBlock": mBlock, "mId": 3, "mCreated": true},
        ];
    }

    function generate(){
        local layout = _gui.createLayoutLine();
        mEntryWidgets = {};
        //Populate with individual tables.
        for(local i = 0; i < PBSDatablockEntryTypes.MAX; i++){
            mEntryWidgets[i] <- {};
        }

        local pbsDatablockLabel = mWin.createLabel();
        pbsDatablockLabel.setDefaultFont(3);
        pbsDatablockLabel.setText("PBS datablock");
        layout.addCell(pbsDatablockLabel);
        addWidget(pbsDatablockLabel);

        local workflowSpinner = WrappedSpinner(mWin, ["specular_ogre" "specular_fresnel" "metallic"]);
        workflowSpinner.attachListener(function(value, oldValue){
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateGlobalWorkflow(mBlock.getWrappedId(), value, oldValue);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, workflowSpinner, "workflow");
        mEntryWidgets[PBSDatablockEntryTypes.GLOBAL].workflowSpinner <- workflowSpinner;

        local macroblockSelector = ::BlockSelector(mWin, BlockType.MACRO);
        macroblockSelector.attachSelectionListener(function(newBlockId, oldBlockId){
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateGlobalBlockSetting(mBlock.getWrappedId(), PBSAction.MACRO_CHANGE, newBlockId, oldBlockId);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, macroblockSelector, "Macroblock");
        mEntryWidgets[PBSDatablockEntryTypes.GLOBAL].macroblockSelector <- macroblockSelector;

        local blendblockSelector = ::BlockSelector(mWin, BlockType.BLEND);
        blendblockSelector.attachSelectionListener(function(newBlockId, oldBlockId){
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateGlobalBlockSetting(mBlock.getWrappedId(), PBSAction.BLEND_CHANGE, newBlockId, oldBlockId);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, blendblockSelector, "Blendblock");
        mEntryWidgets[PBSDatablockEntryTypes.GLOBAL].blendblockSelector <- blendblockSelector;

        local shadowConstBias = ::NumericTextField(mWin);
        shadowConstBias.attachValueChangeListener(function(value, oldValue){
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateShadowConstBias(mBlock.getWrappedId(), value, oldValue);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, shadowConstBias, "shadow const bias");
        mEntryWidgets[PBSDatablockEntryTypes.GLOBAL].shadowConstBias <- shadowConstBias;

        { //Transparency
            local text = mWin.createLabel();
            text.setDefaultFont(3);
            text.setText("Transparency");
            layout.addCell(text);
            addWidget(text);

            local slider = ::SliderObject(mWin);
            slider.attachValueChangeListener(function(type, value, oldValue){
                if(type == SliderObjectListener.VALUE_VIEWED){
                    mBlock.setTransparency(value, true);
                }
                else if(type == SliderObjectListener.VALUE_CHANGED){
                    local action = ::g.ActionManager.PBSDatablockAction();
                    action.populateSingleFloat(mBlock.getWrappedId(), PBSActionSingleValue.TRANSPARENCY, value, oldValue);
                    ::g.ActionManager.pushAndPerform(action);
                }
            }, this);
            addWidgetWithLabel(layout, slider, "value");
            mEntryWidgets[PBSDatablockEntryTypes.TRANSPARENCY].value <- slider;

            local spinner = WrappedSpinner(mWin, ["None", "Transparent", "Fade", "Refractive"]);
            spinner.attachListener(function(value, oldValue){
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateSetTransparentMode(mBlock.getWrappedId(), value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, spinner, "mode");
            mEntryWidgets[PBSDatablockEntryTypes.TRANSPARENCY].mode <- spinner;

            local checkbox = mWin.createCheckbox();
            checkbox.setText("use alpha from textures");
            checkbox.attachListener(function(widget, action){
                if(action != 3) return;
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateUseAlphaFromTexture(mBlock.getWrappedId(), widget.getValue());
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, checkbox, null);
            mEntryWidgets[PBSDatablockEntryTypes.TRANSPARENCY].useAlphaFromTextures <- checkbox;
        }

        createDetailLayer(0, PBSDatablockEntryTypes.DETAIL_0, layout, mDetailDiffuseData[0]);
        createDetailLayer(1, PBSDatablockEntryTypes.DETAIL_1, layout, mDetailDiffuseData[1]);
        createDetailLayer(2, PBSDatablockEntryTypes.DETAIL_2, layout, mDetailDiffuseData[2]);
        createDetailLayer(3, PBSDatablockEntryTypes.DETAIL_3, layout, mDetailDiffuseData[3]);

        createThreeFloatOption("Diffuse", layout, PBSDatablockEntryTypes.DIFFUSE, _PBSM_DIFFUSE, mDiffuseData);
        createThreeFloatOption("Specular", layout, PBSDatablockEntryTypes.SPECULAR, _PBSM_SPECULAR, mSpecularData);

        createSimpleOption("Roughness", layout, PBSDatablockEntryTypes.ROUGHNESS, _PBSM_ROUGHNESS, mRoughnessData);
        createSimpleOption("Metalness", layout, PBSDatablockEntryTypes.METALNESS, _PBSM_METALLIC, mMetalnessData);
        createSimpleOption("Normal", layout, PBSDatablockEntryTypes.NORMAL, _PBSM_NORMAL, mNormalData);

        createThreeFloatOption("Emissive", layout, PBSDatablockEntryTypes.EMISSIVE, _PBSM_EMISSIVE, mEmissiveData);

        { //Fresnel
            local text = mWin.createLabel();
            text.setDefaultFont(3);
            text.setText("Fresnel");
            layout.addCell(text);
            addWidget(text);

            local spinner = WrappedSpinner(mWin, ["coeff" "ior" "coloured" "coloured_ior"]);
            spinner.attachListener(function(value, oldValue){
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateFresnelModeChange(mBlock.getWrappedId(), value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, spinner, "mode");
            mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].mode <- spinner;

            local colourPicker = ::ColourPicker(mWin);
            addWidgetWithLabel(layout, colourPicker, "value");
            colourPicker.attachColourListener(function(type, values, oldValues){
                print("colour changed " + values);
                local action = ::g.ActionManager.PBSDatablockAction();
                local newCopy = values.slice(0, values.len());
                local oldCopy = oldValues.slice(0, oldValues.len());
                assert(newCopy.len() == 3);
                action.populate3fColour(mBlock.getWrappedId(), PBSColourChanged.FRESNEL, newCopy, oldCopy);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].colour <- colourPicker;

            local texSelector = ::TextureSelector(mWin);
            texSelector.attachSelectionListener(function(texName, oldTexName){
                print("texture selected " + texName);
                local action = ::g.ActionManager.PBSDatablockAction();
                //NOTE The Ogre pbs json parser codebase sets specular for fresnel settings, infact there's no fresnel texture type.
                action.populateSetTexture(mBlock.getWrappedId(), PBSAction.TEXTURE_CHANGE, _PBSM_SPECULAR, texName, oldTexName);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, texSelector, "texture");
            mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].texture <- texSelector;

            local samplerSelector = ::BlockSelector(mWin, BlockType.SAMPLER);
            addWidgetWithLabel(layout, samplerSelector, "sampler");
            //TODO actually implement these.
            mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].sampler <- samplerSelector;

            local editBox = ::NumericTextField(mWin);
            editBox.attachValueChangeListener(function(value, oldValue){
                print("UV value change " + value);
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateUVChange(mBlock.getWrappedId(), _PBSM_SPECULAR, value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, editBox, "uv");
            mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].uv <- editBox;
        }

        layout.layout();
        mWin.sizeScrollToFit();
    }

    function createDetailLayer(id, type, layout, actionData){
        local text = mWin.createLabel();
        text.setDefaultFont(3);
        text.setText("Detail diffuse " + id);
        layout.addCell(text);
        addWidget(text);

        if(actionData.mCreated){
            local workflowSpinner = WrappedSpinner(mWin, ["NormalNonPremul","NormalPremul","Add","Subtract","Multiply","Multiply2x","Screen","Overlay","Lighten","Darken","GrainExtract","GrainMerge","Difference"]);
            workflowSpinner.attachListener(function(value, oldValue){
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateDetailLayerSetMode(mBlock.getWrappedId(), mId, value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, actionData);
            addWidgetWithLabel(layout, workflowSpinner, "mode");

            local slider = ::SliderObject(mWin);
            slider.attachValueChangeListener(function(type, value, oldValue){
                if(type == SliderObjectListener.VALUE_VIEWED){
                }
                else if(type == SliderObjectListener.VALUE_CHANGED){
                    local action = ::g.ActionManager.PBSDatablockAction();
                    action.populateDetailLayerSetWeight(mBlock.getWrappedId(), mId, value, oldValue);
                    ::g.ActionManager.pushAndPerform(action);
                }
            }, actionData);
            addWidgetWithLabel(layout, slider, "value");
            mEntryWidgets[type].value <- slider;

            local texSelector = ::TextureSelector(mWin);
            texSelector.attachSelectionListener(function(texName, oldTexName){
                print("texture selected " + texName);
                local action = ::g.ActionManager.PBSDatablockAction();
                local vals = [_PBSM_DETAIL0, _PBSM_DETAIL1, _PBSM_DETAIL2, _PBSM_DETAIL3];

                action.populateSetTexture(mBlock.getWrappedId(), PBSAction.TEXTURE_CHANGE, vals[mId], texName, oldTexName);
                ::g.ActionManager.pushAndPerform(action);
            }, actionData);
            addWidgetWithLabel(layout, texSelector, "texture");
            mEntryWidgets[type].texture <- texSelector;

            local detailLayerSamplerSelector = ::BlockSelector(mWin, BlockType.SAMPLER);
            addWidgetWithLabel(layout, detailLayerSamplerSelector, "sampler");
            mEntryWidgets[type].sampler <- detailLayerSamplerSelector;

            local editBox = ::NumericTextField(mWin);
            editBox.attachValueChangeListener(function(value, oldValue){
                //TODO remove duplication.
                local vals = [_PBSM_DETAIL0, _PBSM_DETAIL1, _PBSM_DETAIL2, _PBSM_DETAIL3];

                print("UV value change " + value);
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateUVChange(mBlock.getWrappedId(), vals[mId], value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, actionData);
            addWidgetWithLabel(layout, editBox, "uv");
            mEntryWidgets[type].uv <- editBox;

            //TODO offset and scale.

        }else{
            local button = mWin.createButton();
            button.setText("Create detail layer");
            button.attachListener(function(widget, action){
                if(action != 2) return;
                print("Creating detail layer");
            }, this);
            layout.addCell(button);
        }
    }

    function createThreeFloatOption(title, layout, type, PBSMVal, actionData){
        local text = mWin.createLabel();
        text.setDefaultFont(3);
        text.setText(title);
        layout.addCell(text);
        addWidget(text);

        local colourPicker = ::ColourPicker(mWin);
        addWidgetWithLabel(layout, colourPicker, "value");
        colourPicker.attachColourListener(function(type, values, oldValues){
            print("colour changed " + values);
            local action = ::g.ActionManager.PBSDatablockAction();
            local newCopy = values.slice(0, values.len());
            local oldCopy = oldValues.slice(0, oldValues.len());
            assert(newCopy.len() == 3);
            action.populate3fColour(mBlock.getWrappedId(), mType, newCopy, oldCopy);
            ::g.ActionManager.pushAndPerform(action);
        }, actionData);
        mEntryWidgets[type].colour <- colourPicker;

        local texSelector = ::TextureSelector(mWin);
        texSelector.attachSelectionListener(function(texName, oldTexName){
            print("texture selected " + texName);
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateSetTexture(mBlock.getWrappedId(), PBSAction.TEXTURE_CHANGE, PBSMVal, texName, oldTexName);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, texSelector, "texture");
        mEntryWidgets[type].texture <- texSelector;

        local diffuseSamplerSelector = ::BlockSelector(mWin, BlockType.SAMPLER);
        addWidgetWithLabel(layout, diffuseSamplerSelector, "sampler");
        mEntryWidgets[type].sampler <- diffuseSamplerSelector;

        local editBox = ::NumericTextField(mWin);
        editBox.attachValueChangeListener(function(value, oldValue){
            print("UV value change " + value);
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateUVChange(mBlock.getWrappedId(), PBSMVal, value, oldValue);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, editBox, "uv");
        mEntryWidgets[type].uv <- editBox;
    }

    function createSimpleOption(title, layout, type, PBSMVal, actionData){
        local text = mWin.createLabel();
        text.setDefaultFont(3);
        text.setText(title);
        layout.addCell(text);
        addWidget(text);

        local slider = ::SliderObject(mWin);
        slider.attachValueChangeListener(function(type, value, oldValue){
            if(type == SliderObjectListener.VALUE_VIEWED){
                if(mType == PBSActionSingleValue.ROUGHNESS) mBlock.setRoughness(value, true);
                else if(mType == PBSActionSingleValue.METALNESS) mBlock.setMetalness(value, true);
                else if(mType == PBSActionSingleValue.NORMAL) mBlock.setNormal(value, true);
                else assert(false);
            }
            else if(type == SliderObjectListener.VALUE_CHANGED){
                local action = ::g.ActionManager.PBSDatablockAction();
                action.populateSingleFloat(mBlock.getWrappedId(), mType, value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }
        }, actionData);
        addWidgetWithLabel(layout, slider, "value");
        mEntryWidgets[type].value <- slider;

        local texSelector = ::TextureSelector(mWin);
        texSelector.attachSelectionListener(function(texName, oldTexName){
            print("texture selected " + texName);
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateSetTexture(mBlock.getWrappedId(), PBSAction.TEXTURE_CHANGE, PBSMVal, texName, oldTexName);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, texSelector, "texture");
        mEntryWidgets[type].texture <- texSelector;

        local roughnessSamplerSelector = ::BlockSelector(mWin, BlockType.SAMPLER);
        addWidgetWithLabel(layout, roughnessSamplerSelector, "sampler");
        mEntryWidgets[type].sampler <- roughnessSamplerSelector;

        local editBox = ::NumericTextField(mWin);
        editBox.attachValueChangeListener(function(value, oldValue){
            print("UV value change " + value);
            local action = ::g.ActionManager.PBSDatablockAction();
            action.populateUVChange(mBlock.getWrappedId(), PBSMVal, value, oldValue);
            ::g.ActionManager.pushAndPerform(action);
        }, this);
        addWidgetWithLabel(layout, editBox, "uv");
        mEntryWidgets[type].uv <- editBox;
    }

    function setDatablockName(name){
        titleName.setText(name);
    }

    function setEntriesForBlock(block){
        mEntryWidgets[PBSDatablockEntryTypes.GLOBAL].workflowSpinner.setValueRaw(block.getWorkflow());
        mEntryWidgets[PBSDatablockEntryTypes.GLOBAL].shadowConstBias.setValue(block.getShadowConstBias());

        mEntryWidgets[PBSDatablockEntryTypes.TRANSPARENCY].value.setValueRaw(block.getTransparency());
        mEntryWidgets[PBSDatablockEntryTypes.TRANSPARENCY].mode.setValueRaw(block.getTransparencyMode());
        print("alpha " + block.getUseAlphaFromTextures());
        mEntryWidgets[PBSDatablockEntryTypes.TRANSPARENCY].useAlphaFromTextures.setValue(block.getUseAlphaFromTextures());

        mEntryWidgets[PBSDatablockEntryTypes.DIFFUSE].colour.setValueRaw(block.getDiffuse());
        mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].colour.setValueRaw(block.getFresnel());
        mEntryWidgets[PBSDatablockEntryTypes.SPECULAR].colour.setValueRaw(block.getSpecular());
        mEntryWidgets[PBSDatablockEntryTypes.EMISSIVE].colour.setValueRaw(block.getEmissive());
        mEntryWidgets[PBSDatablockEntryTypes.METALNESS].value.setValueRaw(block.getMetalness());
        mEntryWidgets[PBSDatablockEntryTypes.ROUGHNESS].value.setValueRaw(block.getRoughness());
        mEntryWidgets[PBSDatablockEntryTypes.NORMAL].value.setValueRaw(block.getNormal());

        mEntryWidgets[PBSDatablockEntryTypes.DIFFUSE].uv.setValue(block.getTextureUVSource(_PBSM_DIFFUSE));
        //NOTE In Ogre's codebase fresnel and specular share the same id.
        mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].uv.setValue(block.getTextureUVSource(_PBSM_SPECULAR));
        mEntryWidgets[PBSDatablockEntryTypes.SPECULAR].uv.setValue(block.getTextureUVSource(_PBSM_SPECULAR));
        mEntryWidgets[PBSDatablockEntryTypes.EMISSIVE].uv.setValue(block.getTextureUVSource(_PBSM_EMISSIVE));
        mEntryWidgets[PBSDatablockEntryTypes.METALNESS].uv.setValue(block.getTextureUVSource(_PBSM_METALLIC));
        mEntryWidgets[PBSDatablockEntryTypes.ROUGHNESS].uv.setValue(block.getTextureUVSource(_PBSM_ROUGHNESS));
        mEntryWidgets[PBSDatablockEntryTypes.NORMAL].uv.setValue(block.getTextureUVSource(_PBSM_NORMAL));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_0].uv.setValue(block.getTextureUVSource(_PBSM_DETAIL0));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_1].uv.setValue(block.getTextureUVSource(_PBSM_DETAIL1));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_2].uv.setValue(block.getTextureUVSource(_PBSM_DETAIL2));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_3].uv.setValue(block.getTextureUVSource(_PBSM_DETAIL3));

        mEntryWidgets[PBSDatablockEntryTypes.DIFFUSE].texture.setTexture(block.getTexture(_PBSM_DIFFUSE));
        mEntryWidgets[PBSDatablockEntryTypes.FRESNEL].texture.setTexture(block.getTexture(_PBSM_SPECULAR));
        mEntryWidgets[PBSDatablockEntryTypes.SPECULAR].texture.setTexture(block.getTexture(_PBSM_SPECULAR));
        mEntryWidgets[PBSDatablockEntryTypes.EMISSIVE].texture.setTexture(block.getTexture(_PBSM_EMISSIVE));
        mEntryWidgets[PBSDatablockEntryTypes.METALNESS].texture.setTexture(block.getTexture(_PBSM_METALLIC));
        mEntryWidgets[PBSDatablockEntryTypes.ROUGHNESS].texture.setTexture(block.getTexture(_PBSM_ROUGHNESS));
        mEntryWidgets[PBSDatablockEntryTypes.NORMAL].texture.setTexture(block.getTexture(_PBSM_NORMAL));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_0].texture.setTexture(block.getTexture(_PBSM_DETAIL0));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_1].texture.setTexture(block.getTexture(_PBSM_DETAIL1));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_2].texture.setTexture(block.getTexture(_PBSM_DETAIL2));
        mEntryWidgets[PBSDatablockEntryTypes.DETAIL_3].texture.setTexture(block.getTexture(_PBSM_DETAIL3));
        //mEntryWidgets[PBSDatablockEntryTypes.DETAIL_NORMAL_0].texture.setTexture(block.getTexture(_PBSM_DETAIL0_NM));
        //mEntryWidgets[PBSDatablockEntryTypes.DETAIL_NORMAL_1].texture.setTexture(block.getTexture(_PBSM_DETAIL1_NM));
        //mEntryWidgets[PBSDatablockEntryTypes.DETAIL_NORMAL_2].texture.setTexture(block.getTexture(_PBSM_DETAIL2_NM));
        //mEntryWidgets[PBSDatablockEntryTypes.DETAIL_NORMAL_3].texture.setTexture(block.getTexture(_PBSM_DETAIL3_NM));
    }
};
