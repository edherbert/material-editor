::g.MacroblockEditorGui <- class extends ::g.ObjectEditorGui{

    mEntryWidgets = null;

    constructor(win, block){
        base.constructor(win, block);

        mEntryWidgets = {};
    }

    function generate(){
        local layout = _gui.createLayoutLine();

        local titleLabel = mWin.createLabel();
        titleLabel.setDefaultFont(3);
        titleLabel.setText("Macroblock");
        layout.addCell(titleLabel);
        addWidget(titleLabel);

        {
            local checkbox = mWin.createCheckbox();
            checkbox.setText("Scissor test");
            checkbox.attachListener(function(widget, action){
                if(action != 3) return;
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.SCISSOR_TEST_CHANGE, widget.getValue(), !widget.getValue());
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, checkbox, null);
            mEntryWidgets.scissorTest <- checkbox;
        }

        {
            local checkbox = mWin.createCheckbox();
            checkbox.setText("Depth check");
            checkbox.attachListener(function(widget, action){
                if(action != 3) return;
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.DEPTH_CHECK, widget.getValue(), !widget.getValue());
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, checkbox, null);
            mEntryWidgets.depthCheck <- checkbox;
        }

        {
            local checkbox = mWin.createCheckbox();
            checkbox.setText("Depth write");
            checkbox.attachListener(function(widget, action){
                if(action != 3) return;
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.DEPTH_WRITE, widget.getValue(), !widget.getValue());
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, checkbox, null);
            mEntryWidgets.depthWrite <- checkbox;
        }

        {
            local spinner = WrappedSpinner(mWin, ["less" "less_equal" "equal" "not_equal" "greater_equal" "greater" "never" "always"]);
            spinner.attachListener(function(value, oldValue){
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.DEPTH_FUNCTION, value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, spinner, "Depth function");
            mEntryWidgets.depthFunction <- spinner;
        }

        {
            local editbox = ::NumericTextField(mWin);
            editbox.attachValueChangeListener(function(value, oldvalue){
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.DEPTH_BIAS_CONSTANT, value, oldvalue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, editbox, "depth bias constant");
            mEntryWidgets.depthBiasConstant <- editbox;
        }

        {
            local editbox = ::NumericTextField(mWin);
            editbox.attachValueChangeListener(function(value, oldvalue){
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.DEPTH_BIAS_SLOPE_SCALE, value, oldvalue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, editbox, "Depth bias slope scale");
            mEntryWidgets.depthBiasSlopeScale <- editbox;
        }

        {
            local spinner = WrappedSpinner(mWin, ["none" "clockwise" "anticlockwise"]);
            spinner.attachListener(function(value, oldValue){
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.CULL_MODE, value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, spinner, "Cull mode");
            mEntryWidgets.cullMode <- spinner;
        }

        {
            local spinner = WrappedSpinner(mWin, ["points" "wireframe" "solid"]);
            spinner.attachListener(function(value, oldValue){
                local action = ::g.ActionManager.MacroblockAction();
                action.populate(mBlock.getWrappedId(), MacroblockAction.POLYGON_MODE, value, oldValue);
                ::g.ActionManager.pushAndPerform(action);
            }, this);
            addWidgetWithLabel(layout, spinner, "Polygon mode");
            mEntryWidgets.polygonMode <- spinner;
        }

        layout.layout();
    }

    function setEntriesForBlock(block){
        mEntryWidgets.scissorTest.setValue(block.mData.scissor_test);
        mEntryWidgets.depthCheck.setValue(block.mData.depth_check);
        mEntryWidgets.depthWrite.setValue(block.mData.depth_write);
        mEntryWidgets.depthFunction.setValueRaw(block.mData.depth_function);
        mEntryWidgets.depthBiasConstant.setValue(block.mData.depth_bias_constant);
        mEntryWidgets.depthBiasSlopeScale.setValue(block.mData.depth_bias_slope_scale);
        mEntryWidgets.cullMode.setValueRaw(block.mData.cull_mode);
        mEntryWidgets.polygonMode.setValueRaw(block.mData.polygon_mode);
    }
};
