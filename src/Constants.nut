enum BlockType{
    PBS,
    UNLIT,

    MACRO,
    BLEND,
    SAMPLER
};

enum Events{
    BLOCKS_CHANGED = 1001,
    EDITED_BLOCK_CHANGED = 1002,
    BLOCK_VALUE_CHANGED = 1003,
    EDITED_BLOCK_REGENERATED = 1004,
    VIEWED_MESH_CHANGED = 1005,
};

enum PBSColourChanged{
    DIFFUSE,
    SPECULAR,
    FRESNEL,
    EMISSIVE
};

//Taken from datablock pbs json settings.
enum PBSFresnelMode{
    coeff,
    ior,
    coloured,
    coloured_ior
};

enum PBSDatablockEntryTypes{
    GLOBAL,
    TRANSPARENCY,
    DIFFUSE,
    SPECULAR,
    ROUGHNESS,
    FRESNEL,
    METALNESS,
    NORMAL,
    REFLECTION,
    EMISSIVE,
    DETAIL_WEIGHT,
    DETAIL_0,
    DETAIL_1,
    DETAIL_2,
    DETAIL_3,
    DETAIL_NORMAL_0,
    DETAIL_NORMAL_1,
    DETAIL_NORMAL_2,
    DETAIL_NORMAL_3,
    MAX
};

const BLOCK_EDITOR_GUI_WIDTH = 600;
const BLOCK_EDITOR_GUI_HEIGHT = 1080;

const BLOCK_VIEWER_GUI_WIDTH = 200;

const TOOLBAR_GUI_HEIGHT = 70;
