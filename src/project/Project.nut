::g.Project <- {

    "mSamplerBlocks": [],
    "mBlendBlocks": [],
    "mMacroBlocks": [],
    "mPBSBlendBlocks": [],
    "mUnlitBlendBlocks": [],

    //Lists all blocks. BlockId is relative to this list and blocks themselves remember their index into the prior lists.
    "mTotalBlocks": [],

    //Store old block ids so they can be re-used later.
    //This is required to keep the undo stack consistant.
    //The idea is that you recycle an id when no longer needing it (block deleted), but if we need to undo then getBlockId will return the same id during the redo.
    "mOldBlockIds": [],
    "mBlockIdsCount": 0,

    function setup(projPath){
        if(projPath == null){
            print("Creating empty project");
        }else{
            loadFromFile(projPath);
        }
    }

    function loadFromFile(path){
        local tableData = null;
        try{
            tableData = _system.readJSONAsTable(path);
        }catch(e){
            print("Error reading from file " + path);
            return;
        }

        if("pbs" in tableData){
            _loadBlockData(tableData["pbs"], BlockType.PBS);
        }
        if("blendblocks" in tableData){
            _loadBlockData(tableData["blendblocks"], BlockType.BLEND);
        }
        if("macroblocks" in tableData){
            _loadBlockData(tableData["macroblocks"], BlockType.MACRO);
        }
    }

    function _loadBlockData(d, type){
        foreach(i,c in d){
            local blockId = createBlock(i, type);
            local block = mTotalBlocks[blockId];
            block.populateFromTable(c);
        }
    }

    function saveToFile(filePath){
        local constructedTable = {
        };

        if(mSamplerBlocks.len() > 0){
            local samplerBlocks = {};

            constructedTable.samplers <- samplerBlocks;
        }
        if(mBlendBlocks.len() > 0){
            local blendBlocks = {};
            foreach(i in mBlendBlocks){
                local table = i.getAsTable();
                blendBlocks[i.mName] <- table;
            }
            constructedTable.blendblocks <- blendBlocks;
        }
        if(mMacroBlocks.len() > 0){
            local macroBlocks = {};
            foreach(i in mMacroBlocks){
                local table = i.getAsTable();
                macroBlocks[i.mName] <- table;
            }
            constructedTable.macroBlocks <- macroBlocks;
        }
        if(mPBSBlendBlocks.len() > 0){
            local pbsBlocks = {};
            foreach(i in mPBSBlendBlocks){
                local table = i.getAsTable();
                pbsBlocks[i.mName] <- table;
            }
            constructedTable.pbs <- pbsBlocks;
        }



        try{
            _system.writeJsonAsFile(filePath, constructedTable);
        }catch(e){
            print("Error saving file to path " + filePath);
            return;
        }

        print("Saved file to " + filePath);
    }

    function getBlockTargets(blockType){
        switch(blockType){
            case BlockType.PBS:
                return [mPBSBlendBlocks, PBSDatablock];
            case BlockType.UNLIT:
                return [mUnlitBlendBlocks, UnlitDatablock];
            case BlockType.MACRO:
                return [mMacroBlocks, Macroblock];
            case BlockType.BLEND:
                return [mBlendBlocks, Blendblock];
            case BlockType.SAMPLER:
                return [mSamplerBlocks, Samplerblock];
            default:{
                print(blockType);
                assert(false);
            }
        }
    }

    function findHoleInArray(a){
        for(local i = 0; i < a.len(); i++){
            if(a[i] == null) return i;
        }
        local ret = a.len();
        //Make the slot there for later.
        a.append(null);
        return ret;
    }

    function createBlock(name, blockType){
        local blockId = getBlockId();

        local blockTargets = getBlockTargets(blockType);
        local targetArray = blockTargets[0];
        local targetBlockClass = blockTargets[1];

        local localArrayHole = findHoleInArray(targetArray);
        local newBlock = targetBlockClass(localArrayHole, name, blockType);

        local totalArrayHole = findHoleInArray(mTotalBlocks);
        targetArray[localArrayHole] = newBlock;
        mTotalBlocks[totalArrayHole] = newBlock;

        print("Creating block with name: '" + name + "' with id " + blockId.tostring());

        _event.transmit(Events.BLOCKS_CHANGED, this);

        return blockId;
    }

    function getBlockForBlockId(id){
        //Get the block from one of the lists.
        local blockTargets = getBlockTargets(id.mType);
        return blockTargets[0][id.mId];
    }

    function removeBlock(blockId){
        recycleBlockId(blockId);
        local targetBlock = mTotalBlocks[blockId];
        mTotalBlocks[blockId] = null;

        //Remove from the local array.
        local results = getBlockTargets(targetBlock.mBlockType);
        (results[0])[targetBlock.mBlockId] = null;

        _event.transmit(Events.BLOCKS_CHANGED, this);
    }

    function getBlockId(){
        if(mOldBlockIds.len() > 0){
            local value = mOldBlockIds.pop();
            return value;
        }
        local value = mBlockIdsCount;
        mBlockIdsCount++;
        return value;
    }
    function recycleBlockId(id){
        mOldBlockIds.append(id);
    }
};
_doFile("res://src/project/ProjectBlock.nut");
_doFile("res://src/project/ProjectPBSDatablock.nut");
_doFile("res://src/project/ProjectUnlitDatablock.nut");
_doFile("res://src/project/ProjectMacroblock.nut");
_doFile("res://src/project/ProjectBlendblock.nut");
_doFile("res://src/project/ProjectSamplerblock.nut");
