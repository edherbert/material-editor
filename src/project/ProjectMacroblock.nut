::g.Project.Macroblock <- class extends ProjectBlock{

    mData = null;

    mDefaultBlock = {
        "scissor_test" : false,
        "depth_check" : true,
        "depth_write" : true,
        "depth_function" : 0,
        "depth_bias_constant" : 0,
        "depth_bias_slope_scale" : 0,
        "cull_mode" : 0,
        "polygon_mode" : 0
    };

    constructor(blockId, name, blockType){
        base.constructor(blockId, name, blockType);

        mData = clone mDefaultBlock;
        generate();
    }

    function generate(){
        mEngineBlock = _hlms.getMacroblock(mData);
    }

    function setScissorTest(val){
        mData.scissor_test = val;
        _valueChanged();
    }
    function setDepthCheck(val){
        mData.depth_check = val;
        _valueChanged();
    }
    function setDepthWrite(val){
        mData.depth_write = val;
        _valueChanged();
    }
    function setDepthFunction(val){
        mData.depth_function = val;
        _valueChanged();
    }
    function setDepthBiasConstant(val){
        mData.depth_bias_constant = val;
        _valueChanged();
    }
    function setDepthBiasSlopeScale(val){
        mData.depth_bias_slope_scale = val;
        _valueChanged();
    }
    function setCullMode(val){
        mData.cull_mode = val;
        _valueChanged();
    }
    function setPolygonMode(val){
        mData.polygon_mode = val;
        _valueChanged();
    }
    function getAsTable(){
        local returnTable = {};

        if(mData.scissor_test != mDefaultBlock.scissor_test) returnTable.scissor_test <- mData.scissor_test;
        if(mData.depth_check != mDefaultBlock.depth_check) returnTable.depth_check <- mData.depth_check;
        if(mData.depth_write != mDefaultBlock.depth_write) returnTable.depth_write <- mData.depth_write;
        if(mData.depth_bias_constant != mDefaultBlock.depth_bias_constant) returnTable.depth_bias_constant <- mData.depth_bias_constant;
        if(mData.depth_bias_slope_scale != mDefaultBlock.depth_bias_slope_scale) returnTable.depth_bias_slope_scale <- mData.depth_bias_slope_scale;

        if(mData.depth_function != mDefaultBlock.depth_function){
            local depthFunctions = ["less" "less_equal" "equal" "not_equal" "greater_equal" "greater" "never" "always"];
            returnTable.depth_function <- depthFunctions[mData.depth_function];
        }
        if(mData.cull_mode != mDefaultBlock.cull_mode){
            local cullMode = ["none" "clockwise" "anticlockwise"];
            returnTable.cull_mode <- cullMode[mData.cull_mode];
        }
        if(mData.polygon_mode != mDefaultBlock.polygon_mode){
            local polyMode = ["points" "wireframe" "solid"];
            returnTable.polygon_mode <- polyMode[mData.polygon_mode];
        }
        return returnTable;
    }

};
