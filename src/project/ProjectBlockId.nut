const BLOCK_ID_INVALID = 0xFFFFFFFF;

//Simple wrapper around a block type and index.
::BlockId <- class{

    //BLOCK_ID_INVALID for either of these means invalid.
    mType = null;
    mId = null;

    constructor(blockType = BLOCK_ID_INVALID, blockId = BLOCK_ID_INVALID){
        mType = blockType;
        mId = blockId;
    }

    function isValid(){
        return mType == BLOCK_ID_INVALID || mId == BLOCK_ID_INVALID;
    }
};
