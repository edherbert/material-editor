::ProjectBlock <- class{

    mBlockType = null;
    mBlockId = null;
    mName = null;
    mEngineBlock = null;

    constructor(id, name, blockType){
        mBlockId = id;
        mName = name;
        mBlockType = blockType;
    }

    function getWrappedId(){
        return ::BlockId(mBlockType, mBlockId);
    }

    function _valueChanged(){
        _event.transmit(Events.BLOCK_VALUE_CHANGED, this);
    }


    /**
    Iterate through the parent table finding keys and values.
    If they exist in the targetData then insert them into the parent.
    This is useful to apply changed datablock values to a default datablock.
    */
    function recursiveApplyToBlock(parentData, targetData){
        foreach(i,c in parentData){
            if(i in targetData){
                if(typeof(parentData[i]) == "table" && typeof(targetData[i]) == "table"){
                    recursiveApplyToBlock(parentData[i], targetData[i]);
                }else{
                    parentData[i] = targetData[i]
                }
            }else{
                //If that key doesn't exist in the parent then just skip it.
                continue;
            }
        }
    }

};
