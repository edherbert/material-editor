::g.Project.PBSDatablock <- class extends ProjectBlock{

    //TODO when these block changes are done, get rid of expectedMetalness as it's stored in the tables.
    mExpectedMetalness = 0.0;
    mData = null;
    workflowVals = ["specular_ogre" "specular_fresnel" "metallic"];

    mDefaultBlock = {
        "macroblock": null,
        "blendblock": null,
        //alpha_test
        "shadow_const_bias": 0.01,

        "workflow": "specular_ogre",

        "transparency":{
            "value": 1.0,
            "mode": "None",
            "use_alpha_from_textures": true
        },
        "diffuse":{
            "value": [1, 1, 1],
            "texture": null,
            "sampler": null,
            "uv": 0,
            "grayscale": false
        },
        "specular":{
            "value": [1, 1, 1],
            "texture": null,
            "sampler": null,
            "uv": 0
        },
        "roughness":{
            "value": 1.0,
            "texture": null,
            "sampler": null,
            "uv": 0
        },
        "fresnel":{
            "mode": "coeff",
            "value": [1.0, 1.0, 1.0],
            "texture": null,
            "sampler": null,
            "uv": 0
        },
        "metallness":{
            "value": 1.0,
            "texture": null,
            "sampler": null,
            "uv": 0
        },
        "normal":{
            "value": 1.0,
            "texture": null,
            "sampler": null,
            "uv": 0
        },
        "emissive":{
            "value": [1, 1, 1],
            "texture": null,
            "sampler": null,
            "uv": 0,
            "lightmap": true
        },
        "detail_weight":{
            "texture": null,
            "sampler": null,
            "uv": 0
        },

        "detail_diffuse0": null,
        "detail_diffuse1": null,
        "detail_diffuse2": null,
        "detail_diffuse3": null,

        "detail_normal0": null,
        "detail_normal1": null,
        "detail_normal2": null,
        "detail_normal3": null,
        //detail_weight
        //reflection

    };

    constructor(blockId, name, blockType){
        base.constructor(blockId, name, blockType);

        local vals = ["detail_diffuse0","detail_diffuse1","detail_diffuse2","detail_diffuse3"];
        foreach(i in vals){
            mDefaultBlock[i] = {
                "mode" : "NormalNonPremul",
                "offset" : [0, 0],
                "scale" : [1, 1],
                "value" : 1,
                "texture" : null,
                "sampler" : null,
                "uv" : 0
            };
        }
        vals = ["detail_normal0","detail_normal1","detail_normal2","detail_normal3"];
        foreach(i in vals){
            mDefaultBlock[i] = {
                //Note, offset and scale are shared with the relevant diffuse layer.
                "offset" : [0, 0],
                "scale" : [1, 1],
                "value" : 1,
                "texture" : null,
                "sampler" : null,
                "uv" : 0
            };
        }

        mData = clone mDefaultBlock;
        mData = fullCloneTable(mDefaultBlock);

        _generateBlock();
        mExpectedMetalness = mEngineBlock.getMetalness();
    }

    function populateFromTable(pbsTable){
        recursiveApplyToBlock(mData, pbsTable);
        setBlockToTable();
    }

    function _setThreeFloatValue(d, texType){
        if("value" in d){
            if(texType == _PBSM_DIFFUSE) mEngineBlock.setDiffuse(d.value[0], d.value[1], d.value[2]);
            else if(texType == _PBSM_SPECULAR) mEngineBlock.setSpecular(d.value[0], d.value[1], d.value[2]);
            else assert(false);
        }
        if("texture" in d) mEngineBlock.setTexture(texType, d.texture);
        if("uv" in d) mEngineBlock.setTextureUVSource(texType, d.uv);
    }
    function _setSimpleValue(d, texType){
        if("value" in d){
            if(texType == _PBSM_ROUGHNESS) mEngineBlock.setRoughness(d.value);
            else if(texType == _PBSM_METALLIC) mEngineBlock.setMetalness(d.value);
            else if(texType == _PBSM_NORMAL) mEngineBlock.setNormalMapWeight(d.value);
            else assert(false);
        }
        if("texture" in d) mEngineBlock.setTexture(texType, d.texture);
        if("uv" in d) mEngineBlock.setTextureUVSource(texType, d.uv);
    }

    function setBlockToTable(){
        //if("macroblock" in mData) mEngineBlock.setMacroblock();

        if("workflow" in mData){
            local index = workflowVals.find(mData["workflow"]);
            if(index != null){
                mEngineBlock.setWorkflow(index);
            }
        }

        if("diffuse" in mData){
            local d = mData["diffuse"];
            _setThreeFloatValue(d, _PBSM_DIFFUSE);
        }
        if("specular" in mData){
            local d = mData["specular"];
            _setThreeFloatValue(d, _PBSM_SPECULAR);
        }
        if("roughness" in mData){
            local d = mData["roughness"];
            _setSimpleValue(d, _PBSM_ROUGHNESS);
        }
        if("metalness" in mData){
            local d = mData["metalness"];
            _setSimpleValue(d, _PBSM_METALLIC);
        }
        if("normal" in mData){
            local d = mData["normal"];
            _setSimpleValue(d, _PBSM_NORMAL);
        }

    }

    function _generateBlock(blend = null, macro = null){
        local engineBlend = null;
        local engineMacro = null;
        if(blend != null){
            engineBlend = blend.mEngineBlock;
        }
        if(macro != null){
            engineMacro = macro.mEngineBlock;
        }

        mEngineBlock = _hlms.getDatablock(mName);
        if(mEngineBlock == null){
            mEngineBlock = _hlms.pbs.createDatablock(mName, engineBlend, engineMacro, {"diffuse": "1 1 1"});
            print(typeof(mEngineBlock));
        }
        assert(typeof(mEngineBlock) == "pbsDatablock");
        ::globalCount++;
    }

    function generateBlock(blend = null, macro = null){
        _generateBlock(blend, macro);
        //TODO I might not need the regenerated thing.
        _event.transmit(Events.EDITED_BLOCK_REGENERATED, mEngineBlock);
    }

    function setShadowConstBias(val){
        mData.shadow_const_bias = val;
        mEngineBlock.setShadowConstBias(val);
        _valueChanged();
    }
    function getShadowConstBias(){
        return mEngineBlock.getShadowConstBias();
    }
    function setBlendblock(blockId){
        local blockInstance = ::g.Project.getBlockForBlockId(blockId);
        assert(blockInstance.mBlockType == BlockType.BLEND);

        mData.blendblock = blockInstance.mName;
        mEngineBlock.setBlendblock(blockInstance.mEngineBlock);
    }
    function setMacroblock(blockId){
        local blockInstance = ::g.Project.getBlockForBlockId(blockId);
        assert(blockInstance.mBlockType == BlockType.MACRO);

        mData.macroblock = blockInstance.mName;
        mEngineBlock.setMacroblock(blockInstance.mEngineBlock);
    }

    function setWorkflow(w){
        //TODO make some of these global.
        mData.workflow = workflowVals[w];

        mEngineBlock.setWorkflow(w);
        if(w == _PBS_WORKFLOW_METALLIC){
            //Set any values incase they were changed between metalness being active.
            mEngineBlock.setMetalness(mExpectedMetalness);
        }
        _valueChanged();
    }
    function getWorkflow(){
        return mEngineBlock.getWorkflow();
    }
    function getTexture(tex){
        return mEngineBlock.getTexture(tex);
    }
    function getTextureUVSource(value){
        return mEngineBlock.getTextureUVSource(value);
    }
    function setTransparency(val, temp = false){
        mData.transparency.value = val;
        mEngineBlock.setTransparency(val, mEngineBlock.getTransparencyMode(), mEngineBlock.getUseAlphaFromTextures(), false);
        if(!temp) _valueChanged();
    }
    function setTransparencyMode(val){
        local values = ["None", "Transparent", "Fade", "Refractive"];
        mData.transparency.mode = values[val];

        mEngineBlock.setTransparency(mEngineBlock.getTransparency(), val, mEngineBlock.getUseAlphaFromTextures(), false);
        _valueChanged();
    }
    function setTransparencyAlphaFromTexture(val){
        mData.transparency.use_alpha_from_textures = val;

        mEngineBlock.setTransparency(mEngineBlock.getTransparency(), mEngineBlock.getTransparencyMode(), val, false);
        _valueChanged();
    }
    function setNormal(val, temp = false){
        mData.normal.value = val;
        mEngineBlock.setNormalMapWeight(val);
        if(!temp) _valueChanged();
    }
    function _getBlockTableForTexType(type){
        switch(type){
            case _PBSM_DIFFUSE: return mData.diffuse;
            case _PBSM_NORMAL: return mData.normal;
            case _PBSM_SPECULAR: return mData.specular;
            case _PBSM_METALLIC: return mData.metallic;
            case _PBSM_ROUGHNESS: return mData.roughness;
            case _PBSM_DETAIL_WEIGHT: return mData.normal;
            case _PBSM_EMISSIVE: return mData.emissive;
            case _PBSM_REFLECTION: return mData.reflection;
            case _PBSM_DETAIL0: return mData.detail_diffuse0;
            case _PBSM_DETAIL1: return mData.detail_diffuse1;
            case _PBSM_DETAIL2: return mData.detail_diffuse2;
            case _PBSM_DETAIL3: return mData.detail_diffuse3;
            case _PBSM_DETAIL0_NM: return mData.detail_normal0;
            case _PBSM_DETAIL1_NM: return mData.detail_normal1;
            case _PBSM_DETAIL2_NM: return mData.detail_normal2;
            case _PBSM_DETAIL3_NM: return mData.detail_normal3;
            {
                assert(false);
                break;
            }
        }
    }
    function setTexture(type, texName){
        _getBlockTableForTexType(type).texture = (texName == "" ? null : texName);
        //In Ogre these two share the same id, so set both values.
        if(type == _PBSM_METALLIC || type == _PBSM_SPECULAR){
            mData.metallness.texture = texName;
            mData.specular.texture = texName;
        }

        mEngineBlock.setTexture(type, texName);
        _valueChanged();
    }
    function setTextureUVSource(texSource, type){
        _getBlockTableForTexType(type).uv = texSource;
        if(type == _PBSM_METALLIC || type == _PBSM_SPECULAR){
            mData.metallness.uv = texSource;
            mData.specular.uv = texSource;
        }

        mEngineBlock.setTextureUVSource(type, texSource);
        _valueChanged();
    }
    function setDiffuse(rgbArray){
        mData.diffuse.value[0] = rgbArray[0];
        mData.diffuse.value[1] = rgbArray[1];
        mData.diffuse.value[2] = rgbArray[2];

        mEngineBlock.setDiffuse(rgbArray[0], rgbArray[1], rgbArray[2]);
        _valueChanged();
    }

    function getDiffuse(){
        return mEngineBlock.getDiffuse();
    }
    function getMetalness(){
        return mEngineBlock.getMetalness();
    }
    function getRoughness(){
        return mEngineBlock.getRoughness();
    }
    function getFresnel(){
        return mEngineBlock.getFresnel();
    }
    function getSpecular(){
        return mEngineBlock.getSpecular();
    }
    function getEmissive(){
        return mEngineBlock.getEmissive();
    }
    function getTransparency(){
        return mEngineBlock.getTransparency();
    }
    function getTransparencyMode(){
        return mEngineBlock.getTransparencyMode();
    }
    function getUseAlphaFromTextures(){
        return mEngineBlock.getUseAlphaFromTextures();
    }
    function getNormal(){
        print(mEngineBlock.getNormalMapWeight());
        return mEngineBlock.getNormalMapWeight();
    }
    function setRoughness(val, temp = false){
        mData.roughness.value = val;
        mEngineBlock.setRoughness(val);
        if(!temp) _valueChanged();
    }

    function setTransparencyFull(transparency, mode, useAlphaFromTextures, changeDatablock){
        mEngineBlock.setTransparency(transparency, mode, useAlphaFromTextures, changeDatablock);
        _valueChanged();
    }

    function setDetailMapBlendMode(idx, mode){
        mDefaultBlock["detail_diffuse" + idx].mode = mode;
        mEngineBlock.setDetailMapBlendMode(idx, mode);
        _valueChanged();
    }
    function setDetailMapOffset(idx, /*Vec2*/offset){
        mDefaultBlock["detail_diffuse" + idx].offset = offset;
        mDefaultBlock["detail_normal" + idx].offset = offset;
        mEngineBlock.setDetailMapOffset(idx, offset);
        _valueChanged();
    }
    function setDetailMapScale(idx, /*Vec2*/scale){
        mDefaultBlock["detail_diffuse" + idx].scale = scale;
        mDefaultBlock["detail_normal" + idx].scale = scale;
        mEngineBlock.setDetailMapScale(idx, scale);
        _valueChanged();
    }
    function setDetailMapWeight(idx, /*float*/weight){
        mDefaultBlock["detail_diffuse" + idx].value = weight;
        mEngineBlock.setDetailMapWeight(idx, weight);
        _valueChanged();
    }
    function setDetailMapNormalWeight(idx, /*float*/weight){
        mDefaultBlock["detail_normal" + idx].value = weight;
        mEngineBlock.setDetailMapNormalWeight(idx, weight);
        _valueChanged();
    }

    function setMetalness(value, temp = false){
        mData.metallness.value = value;
        //Ogre triggers an assert if metalness isn't the active workflow.
        mExpectedMetalness = value;
        if(mEngineBlock.getWorkflow() != _PBS_WORKFLOW_METALLIC) return;

        mEngineBlock.setMetalness(value);
        if(!temp) _valueChanged();
    }
    function setEmissive(rgbArray){
        mData.emissive.value[0] = rgbArray[0];
        mData.emissive.value[1] = rgbArray[1];
        mData.emissive.value[2] = rgbArray[2];

        mEngineBlock.setEmissive(rgbArray[0], rgbArray[1], rgbArray[2]);
        _valueChanged();
    }

    function setFresnel(rgbArray, separateFresnel = null){
        //If the user doesn't supply a separate fresnel parameter then use the one that's already there.
        local separate = false;
        if(separateFresnel == null){
            separate = mEngineBlock.hasSeparateFresnel();
        }else{
            separate = separateFresnel;
        }
        mEngineBlock.setFresnel(rgbArray[0], rgbArray[1], rgbArray[2], separate);
        _valueChanged();
    }
    function setIndexOfRefraction(rgbArray, separateFresnel = null){
        //Similar to setFresnel.
        if(separateFresnel == null){
            local separate = mEngineBlock.hasSeparateFresnel();
        }else{
            separate = separateFresnel;
        }
        mEngineBlock.setIndexOfRefraction(rgbArray[0], rgbArray[1], rgbArray[2], separate);
        _valueChanged();
    }
    function setFresnelMode(mode){
        mData.fresnel.mode = mode;
        //Taken from the Ogre json parser code base.
        local useIOR = false;
        local isColoured = false;
        switch(mode){
            case PBSFresnelMode.coeff:{
                useIOR = false;
                isColoured = false;
                break;
            }
            case PBSFresnelMode.ior:{
                useIOR = true;
                isColoured = false;
                break;
            }
            case PBSFresnelMode.coloured:{
                useIOR = false;
                isColoured = true;
                break;
            }
            case PBSFresnelMode.coloured_ior:{
                useIOR = true;
                isColoured = true;
                break;
            }
        }

        local value = mEngineBlock.getFresnel();
        if(!useIOR)
            mEngineBlock.setFresnel(value.x, value.y, value.z, isColoured);
        else
            mEngineBlock.setIndexOfRefraction(value.x, value.y, value.z, isColoured);

        _valueChanged();
    }
    function setSpecular(rgbArray){
        mEngineBlock.setSpecular(rgbArray[0], rgbArray[1], rgbArray[2]);
        mData.specular.value[0] = rgbArray[0];
        mData.specular.value[1] = rgbArray[1];
        mData.specular.value[2] = rgbArray[2];
        _valueChanged();
    }

    function _arraysEqual(a, b){
        for(local i = 0; i < a.len(); i++){
            if(a[i] != b[i]) return false;
        }
        return true;
    }
    function _addValueToTable(table, parentName, type){
        local entry = {};

        if(
            type == PBSDatablockEntryTypes.DIFFUSE ||
            type == PBSDatablockEntryTypes.SPECULAR ||
            type == PBSDatablockEntryTypes.FRESNEL ||
            type == PBSDatablockEntryTypes.EMISSIVE
        ){
            if(!_arraysEqual(mData[parentName].value, mDefaultBlock[parentName].value)){
                entry.value <- clone mData[parentName].value;
            }
        }
        else if(
            type == PBSDatablockEntryTypes.ROUGHNESS ||
            type == PBSDatablockEntryTypes.METALNESS ||
            type == PBSDatablockEntryTypes.NORMAL
        ){
            if(mData[parentName].value != mDefaultBlock[parentName].value){
                entry.value <- mData[parentName].value;
            }
        }

        if(mData[parentName].texture != mDefaultBlock[parentName].texture){
            entry.texture <- mData[parentName].texture;
        }
        if(mData[parentName].sampler != mDefaultBlock[parentName].sampler){
            entry.sampler <- mData[parentName].sampler;
        }
        if(mData[parentName].uv != mDefaultBlock[parentName].uv){
            entry.uv <- mData[parentName].uv;
        }

        if(entry.len() > 0){
            table[parentName] <- entry;
        }
    }

    function getAsTable(){
        local out = {};

        if(mData.macroblock != mDefaultBlock.macroblock){
            out.macroblock <- mData.macroblock;
        }
        if(mData.blendblock != mDefaultBlock.blendblock){
            out.blendblock <- mData.blendblock;
        }

        if(mData.workflow != mDefaultBlock.workflow) out.workflow <- mData.workflow;
        if(mData.shadow_const_bias != mDefaultBlock.shadow_const_bias) out.shadow_const_bias <- mData.shadow_const_bias;

        {
            local entry = {};
            local t = mData.transparency;
            if(t.value != mDefaultBlock.transparency.value) entry.value <- t.value;
            if(t.mode != mDefaultBlock.transparency.mode) entry.mode <- t.mode;
            if(t.use_alpha_from_textures != mDefaultBlock.transparency.use_alpha_from_textures) entry.use_alpha_from_textures <- t.use_alpha_from_textures;

            if(entry.len() > 0){
                out.transparency <- entry;
            }
        }

        _addValueToTable(out, "diffuse", PBSDatablockEntryTypes.DIFFUSE);
        _addValueToTable(out, "specular", PBSDatablockEntryTypes.SPECULAR);
        _addValueToTable(out, "roughness", PBSDatablockEntryTypes.ROUGHNESS);
        _addValueToTable(out, "metallness", PBSDatablockEntryTypes.METALNESS);
        _addValueToTable(out, "normal", PBSDatablockEntryTypes.NORMAL);
        _addValueToTable(out, "fresnel", PBSDatablockEntryTypes.FRESNEL);
        _addValueToTable(out, "emissive", PBSDatablockEntryTypes.EMISSIVE);

        //TODO write fresnel values.

        return out;
    }

    //Clone deep enough for the pbs settings.
    function fullCloneTable(t){
        local parentTable = clone t;
        foreach(i,c in t){
            if(typeof(c) == "table"){
                local clonedTable = clone c;
                foreach(ii,cc in clonedTable){
                    if(typeof(cc) == "array"){
                        clonedTable[ii] = clone cc;
                    }
                }
                parentTable[i] = clonedTable;
            }
        }
        return parentTable;
    }

};
