::g.Project.Blendblock <- class extends ProjectBlock{

    mData = null;
    mEngineBlock = null;

    //Default values taken from Ogre.
    mDefaultBlock = {
        "alpha_to_coverage" : false,
        "blendmask" : "rgba",
        "separate_blend" : false,
        "src_blend_factor" : _HLMS_SBF_ONE,
        "dst_blend_factor" : _HLMS_SBF_ZERO,
        "src_alpha_blend_factor" : _HLMS_SBF_ONE,
        "dst_alpha_blend_factor" : _HLMS_SBF_ZERO,
        "blend_operation" : _HLMS_SBO_ADD,
        "blend_operation_alpha" : _HLMS_SBO_ADD
    };

    constructor(blockId, name, blockType){
        base.constructor(blockId, name, blockType);

        mData = clone mDefaultBlock;
        generate();
    }
    function setAlphaToCoverage(val){
        print("set alpha " + val);
        mData.alpha_to_coverage = val;
        regenerateBlock();
    }
    function setSeperateBlend(val){
        print("set seperate blend" + val);
        mData.separate_blend = val;
        regenerateBlock();
    }
    function setBlendFactor(type, val){
        print("set blend factor" + val);
        if(type == BlendblockActionFactorChange.SRC) mData.src_blend_factor = val;
        else if(type == BlendblockActionFactorChange.DEST) mData.dst_blend_factor = val;
        else if(type == BlendblockActionFactorChange.SRC_ALPHA) mData.src_alpha_blend_factor = val;
        else if(type == BlendblockActionFactorChange.SRC_DEST) mData.src_alpha_blend_factor = val;
        else assert(false);
        regenerateBlock();
    }
    function setBlendOperation(type, val){
        print("set blend operation " + val);
        if(type == BlendblockActionBlendOperationChange.OPERATION) mData.blend_operation = val;
        else if(type == BlendblockActionBlendOperationChange.ALPHA) mData.blend_operation_alpha = val;
        else assert(false);
        regenerateBlock();
    }

    function regenerateBlock(){
        generate();
        _valueChanged();
    }

    function populateFromTable(pbsTable){
        recursiveApplyToBlock(mData, pbsTable);
        _resolveStrings();
    }

    /**
    If loaded from file values might be stored as strings, where they should be integers.
    */
    function _resolveStrings(){
        foreach(i,c in mData){
            if(typeof(c) != "string") continue;

            if(i == "blend_operation" || i == "blend_operation_alpha"){
                mData[i] = _getBlendOperatorForString(c);
            }else{
                mData[i] = _getFactorForString(c);
            }
        }
    }

    function generate(){
        print("generating block");
        mEngineBlock = _hlms.getBlendblock(mData);
    }

    function _getStringForFactor(factor){
        switch(factor){
            case _HLMS_SBF_ONE: return "one"
            case _HLMS_SBF_ZERO: return "zero"
            case _HLMS_SBF_DEST_COLOUR: return "dst_colour"
            case _HLMS_SBF_SOURCE_COLOUR: return "src_colour"
            case _HLMS_SBF_ONE_MINUS_DEST_COLOUR: return "one_minus_dst_colour"
            case _HLMS_SBF_ONE_MINUS_SOURCE_COLOUR: return "one_minus_src_colour"
            case _HLMS_SBF_DEST_ALPHA: return "dst_alpha"
            case _HLMS_SBF_SOURCE_ALPHA: return "src_alpha"
            case _HLMS_SBF_ONE_MINUS_DEST_ALPHA: return "one_minus_dst_alpha"
            case _HLMS_SBF_ONE_MINUS_SOURCE_ALPHA: return "one_minus_src_alpha"
            default: assert(false);
        }
    }
    function _getStringForBlendOperation(operation){
        switch(operation){
            case _HLMS_SBO_ADD: return "add";
            case _HLMS_SBO_SUBTRACT: return "subtract";
            case _HLMS_SBO_REVERSE_SUBTRACT: return "reverse_subtract";
            case _HLMS_SBO_MIN: return "min";
            case _HLMS_SBO_MAX: return "max";
            default: assert(false);
        }
    }

    function _getFactorForString(o){
        if(o == "one") return _HLMS_SBF_ONE;
        else if(o == "zero") return _HLMS_SBF_ZERO;
        else if(o == "dst_colour") return _HLMS_SBF_DEST_COLOUR;
        else if(o == "src_colour") return _HLMS_SBF_SOURCE_COLOUR;
        else if(o == "one_minus_dst_colour") return _HLMS_SBF_ONE_MINUS_DEST_ALPHA;
        else if(o == "one_minus_src_colour") return _HLMS_SBF_ONE_MINUS_SOURCE_COLOUR;
        else if(o == "dst_alpha") return _HLMS_SBF_DEST_ALPHA;
        else if(o == "src_alpha") return _HLMS_SBF_SOURCE_ALPHA;
        else if(o == "one_minus_dst_alpha") return _HLMS_SBF_ONE_MINUS_DEST_ALPHA;
        else if(o == "one_minus_src_alpha") return _HLMS_SBF_ONE_MINUS_SOURCE_ALPHA;

        return _HLMS_SBF_ONE;
    }
    function _getBlendOperatorForString(o){
        if(o == "add") return _HLMS_SBO_ADD;
        else if(o == "subtract") return _HLMS_SBO_SUBTRACT;
        else if(o == "reverse_subtract") return _HLMS_SBO_REVERSE_SUBTRACT;
        else if(o == "min") return _HLMS_SBO_MIN;
        else if(o == "max") return _HLMS_SBO_MAX;

        return _HLMS_SBO_ADD;
    }

    /**
    From the block data generate and return a table object which can be written to a file suitable for Ogre.
    */
    function getAsTable(){
        local returnTable = {};
        if(mData.alpha_to_coverage != mDefaultBlock.alpha_to_coverage) returnTable.alpha_to_coverage <- mData.alpha_to_coverage;
        if(mData.blendmask != mDefaultBlock.blendmask) returnTable.blendmask <- mData.blendmask;
        if(mData.separate_blend != mDefaultBlock.separate_blend) returnTable.separate_blend <- mData.separate_blend;
        if(mData.src_blend_factor != mDefaultBlock.src_blend_factor) returnTable.src_blend_factor <- _getStringForFactor(mData.src_blend_factor);
        if(mData.dst_blend_factor != mDefaultBlock.dst_blend_factor) returnTable.dst_blend_factor <- _getStringForFactor(mData.dst_blend_factor);
        if(mData.src_alpha_blend_factor != mDefaultBlock.src_alpha_blend_factor) returnTable.src_alpha_blend_factor <- _getStringForFactor(mData.src_alpha_blend_factor);
        if(mData.dst_alpha_blend_factor != mDefaultBlock.dst_alpha_blend_factor) returnTable.dst_alpha_blend_factor <- _getStringForFactor(mData.dst_alpha_blend_factor);

        if(mData.blend_operation != mDefaultBlock.blend_operation) returnTable.blend_operation <- _getStringForFactor(mData.blend_operation);
        if(mData.blend_operation_alpha != mDefaultBlock.blend_operation_alpha) returnTable.blend_operation_alpha <- _getStringForFactor(mData.blend_operation_alpha);

        return returnTable;
    }

};
